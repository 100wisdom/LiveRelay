/*
 * RelayChanneltable.cpp
 *
 *  Created on: 2016年4月30日
 *      Author: terry
 */

#include "RelayChanneltable.h"
#include "TSingleton.h"


namespace av
{

typedef comn::Singleton< RelayChanneltable >	RelayChanneltableSingleton;

RelayChanneltable& RelayChanneltable::instance()
{
	return RelayChanneltableSingleton::instance();
}


RelayChanneltable::RelayChanneltable()
{
}

RelayChanneltable::~RelayChanneltable()
{
}

bool RelayChanneltable::exists(const std::string& name)
{
	RelayChannelPtr chl = findWithName(name);
	return (chl.get() != NULL);
}

struct EqualToName : public std::binary_function< void*, RelayChannelPtr, bool >
{
	EqualToName(const std::string& name):
		m_name(name)
	{
	}

	bool operator () (void* handle, RelayChannelPtr& chl) const
	{
        if (!chl)
        {
            return false;
        }
		return m_name == chl->getName();
	}

	std::string m_name;

};


RelayChannelPtr RelayChanneltable::findWithName(const std::string& name)
{
	EqualToName func(name);
	void* handle = NULL;
	RelayChannelPtr chl;
	findIf(func, handle, chl);
	return chl;
}





} /* namespace av */
