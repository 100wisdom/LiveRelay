/*
 * RelayChanneltable.h
 *
 *  Created on: 2016年4月30日
 *      Author: terry
 */

#ifndef RELAYCHANNELTABLE_H_
#define RELAYCHANNELTABLE_H_

#include "RelayChannel.h"
#include "SharedPtr.h"
#include "TMap.h"

namespace av
{

typedef std::shared_ptr< RelayChannel >		RelayChannelPtr;


class RelayChanneltable : public comn::Map< void*, RelayChannelPtr >
{
public:
	static RelayChanneltable& instance();

public:
	RelayChanneltable();
	virtual ~RelayChanneltable();

	bool exists(const std::string& name);

	RelayChannelPtr findWithName(const std::string& name);

};


} /* namespace av */

#endif /* RELAYCHANNELTABLE_H_ */
