CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

include_directories(${COMN_DIR}/include)
include_directories(include)
include_directories(${PROJECT_ROOT}/RtspCaster)
include_directories(${PROJECT_ROOT}/RtpCaster)

link_directories(${COMN_DIR}/lib)
link_directories(${PROJECT_ROOT}/lib)


aux_source_directory(. SOURCES)


add_library(RtspRelay SHARED ${SOURCES})

target_link_libraries(RtspRelay
	${LIB_PLATFORM}
	debug comnd optimized comn
	RtspCaster RtpCaster
	)

set_target_properties(RtspRelay PROPERTIES LINK_INTERFACE_LIBRARIES "")
set_target_properties(RtspRelay PROPERTIES INTERFACE_LINK_LIBRARIES "")

install(TARGETS RtspRelay DESTINATION .)