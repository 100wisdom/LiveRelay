/*
 * RtspRelay.h
 *
 *  Created on: 2016年4月29日
 *      Author: terry
 */

#ifndef RTSPRELAY_H_
#define RTSPRELAY_H_


#include "RtspCaster.h"
#include "RtpCaster.h"

////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////
#ifdef __cplusplus
extern "C"
{
#endif

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////



/**
 * 初始化
 * @param port	RTSP端口
 * @return 0 表示成功
 */
DLLEXPORT int relay_init(int port);

/**
 * 反初始化
 * @return
 */
DLLEXPORT int relay_quit();

/**
 * 设置端口范围
 * @param minPort
 * @param maxPort
 * @return
 */
DLLEXPORT int relay_set_port_range(int minPort, int maxPort);

/**
 * 打开通道
 * @param handle 返回的通道句柄
 * @param name	通道名称
 * @param fmt   媒体格式
 * @return 0 表示成功
 */
DLLEXPORT int relay_chl_open(HANDLE* handle, int protocol, const char* name, const MFormat* fmt);

/**
 * 关闭通道
 * @param handle 通道句柄
 */
DLLEXPORT void relay_chl_close(HANDLE handle);

/**
 * 通道是否打开, 即判断有效
 * @param handle 通道句柄
 * @return > 0 表示有效
 */
DLLEXPORT int relay_chl_is_open(HANDLE handle);

/**
 * 获取通道媒体格式
 * @param handle   通道句柄
 * @param fmt 媒体格式
 * @return 0 表示成功
 */
DLLEXPORT int relay_chl_get_format(HANDLE handle, MFormat* fmt);

/**
 * 获取本地传输描述
 * @param handle
 * @param url		描述
 * @param size		描述长度
 * @return
 */
DLLEXPORT int relay_chl_get_transport(HANDLE handle, char* videoUrl, int videoSize, char* audioUrl, int audioSize);


/**
 * 设置媒体源
 * @param handle
 * @param url	媒体源URL
 * @return 0 表示成功
 */
DLLEXPORT int relay_chl_set_source(HANDLE handle, const char* videoUrl, const char* audioUrl);


/**
 * 设置事件回调句柄
 * @param cb		回调函数指针
 * @param context	回调环境
 */
DLLEXPORT void relay_set_event_callback(RtspCasterEventCallback cb, void* context);

/**
 * 根据名称查找通道
 * @param name	通道名称
 * @return 通道句柄, NULL 表示没有找到
 */
DLLEXPORT HANDLE relay_chl_find(const char* name);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif


#endif /* RTSPRELAY_H_ */
