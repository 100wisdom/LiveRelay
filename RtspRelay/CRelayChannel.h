/*
 * CRelayChannel.h
 *
 *  Created on: 2016年4月30日
 *      Author: terry
 */

#ifndef CRELAYCHANNEL_H_
#define CRELAYCHANNEL_H_

#include "BasicType.h"
#include "RelayChannel.h"
#include "RtpMediaFormat.h"


namespace av
{

class CRelayChannel: public RelayChannel
{
public:
	CRelayChannel();
	virtual ~CRelayChannel();

	virtual int open(int protocol, const std::string& name, const MFormat& fmt);

	virtual std::string getName();

	virtual void close();

	virtual bool isOpen();

	virtual int setSource(const std::string& videoUrl, const std::string& audioUrl);

	virtual bool getFormat(MFormat* fmt);

	virtual HANDLE getRtspCaster();

	virtual std::string getTransport(std::string& audioUrl);


	void rtpSinkCallback(HANDLE handle, uint8_t* data, int size, int64_t pts, int flags);

	void audioRtpSinkCallback(HANDLE handle, uint8_t* data, int size, int64_t pts, int flags);


	int setVideoSource(const std::string& videoUrl);
	int setAudioSource(const std::string& audioUrl);

protected:
	int	m_protocol;
	std::string	m_name;
	HANDLE	m_rtsp;
	HANDLE	m_videoRtp;
	HANDLE  m_audioRtp;
	int		m_videoPort;
	int		m_audioPort;

	RtpMediaFormat	m_videoRtpFormat;
	RtpMediaFormat	m_audioRtpFormat;
	MFormat	m_format;

    int m_videoPktCount;
    int m_audioPktCount;

};


} /* namespace av */

#endif /* CRELAYCHANNEL_H_ */
