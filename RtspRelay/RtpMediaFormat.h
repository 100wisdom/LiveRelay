/*
 * RtpMediaFormat.h
 *
 *  Created on: 2016年5月1日
 *      Author: terry
 */

#ifndef RTPMEDIAFORMAT_H_
#define RTPMEDIAFORMAT_H_

#include "BasicType.h"
#include <string>


class RtpMediaFormat
{
public:
	static const char* AUDIO;
	static const char* VIDEO;

	static const char*	RTP_OVER_TCP;
	static const char*	RTP_OVER_UDP;

	static int makePayload(const std::string& codec);

public:
	RtpMediaFormat();
	virtual ~RtpMediaFormat();

	std::string m_protocol;

	std::string m_ip;
	int m_port;

	std::string	m_name;		/// media name
	std::string m_codec;
	int m_clockRate;
	int m_payload;
	int m_channels;

	std::string m_fmtp;

	std::string toUrl() const;

	bool parse(const std::string& url);

	bool empty() const;

};


#endif /* RTPMEDIAFORMAT_H_ */
