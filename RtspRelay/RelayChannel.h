/*
 * RelayChannel.h
 *
 *  Created on: 2016年4月30日
 *      Author: terry
 */

#ifndef RELAYCHANNEL_H_
#define RELAYCHANNEL_H_

#include "RtspRelay.h"
#include <string>


namespace av
{

class RelayChannel
{
public:
	virtual ~RelayChannel() {}

	virtual int open(int protocol, const std::string& name, const MFormat& fmt) =0;

	virtual std::string getName() =0;

	virtual void close() =0;

	virtual bool isOpen() =0;

	virtual int setSource(const std::string& videoUrl, const std::string& audioUrl) =0;

	virtual bool getFormat(MFormat* fmt) =0;

	virtual HANDLE getRtspCaster() =0;

	virtual std::string getTransport(std::string& audioUrl) =0;

};



} /* namespace av */

#endif /* RELAYCHANNEL_H_ */
