/*
 * MediaCodecName.h
 *
 *  Created on: 2016年5月1日
 *      Author: terry
 */

#ifndef MEDIACODECNAME_H_
#define MEDIACODECNAME_H_

#include "MediaType.h"
#include <string>

namespace av
{

class MediaCodecName
{
public:
	MediaCodecName();
	virtual ~MediaCodecName();

	static const char* getName(int codec);

	static int codecFromName(const std::string& name);

};

} /* namespace av */

#endif /* MEDIACODECNAME_H_ */
