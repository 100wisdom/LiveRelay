/*
 * CCasterChannel.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef CCASTERCHANNEL_H_
#define CCASTERCHANNEL_H_

#include "CasterChannel.h"
#include "MediaSourceT.h"
#include "RtpMediaCaster.h"


namespace av
{

class CCasterChannel: public CasterChannel
{
public:
	CCasterChannel();
	virtual ~CCasterChannel();

	virtual int open(int codec, int clockRate, int outPort, int ssrc);

	virtual void close();

	virtual bool isOpen();

	virtual int write(MediaPacketPtr& pkt);

	virtual int addTarget(int protocol, const char* ip, int port);

	virtual int removeTarget(int protocol, const char* ip, int port);

	virtual int removeAllTarget(int protocol);

protected:
	void fireMediaPacket(MediaPacketPtr& pkt);

protected:
	int m_codec;
	int m_clockRate;
	int m_outPort;
	int m_ssrc;
	int	m_payload;

	RtpMediaCasterPtr	m_udpCaster;
	RtpMediaCasterPtr	m_tcpCaster;

};



} /* namespace av */

#endif /* CCASTERCHANNEL_H_ */
