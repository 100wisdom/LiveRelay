/*
 * PSRtpPackager.cpp
 *
 *  Created on: 2015年7月5日
 *      Author: terry
 */

#include "PSRtpPackager.h"

namespace av
{




PSRtpPackager::PSRtpPackager():
    m_lastPktTime()
{
}

PSRtpPackager::~PSRtpPackager()
{
}

void PSRtpPackager::slice(const MediaPacket& pkt, int maxSize, RtpPackagerSink* pSink)
{
	/// not used
}

bool PSRtpPackager::join(const RtpPacket& pktIn, MediaPacket& pktOut)
{
	//实现PS流到H.264流的解析

    ProgramStreamParser::StreamPacket parserPkt;
	if (!m_parser.inputData(pktIn.data, pktIn.size, parserPkt))
	{
		return false;
	}

	pktOut.type = MEDIA_TYPE_VIDEO;
	pktOut.data = const_cast< uint8_t*> (parserPkt.data);
	pktOut.size = parserPkt.length;
	pktOut.pts = pktIn.ts;
    pktOut.duration = 0;

    m_lastPktTime = pktIn.ts;

	if (parserPkt.type == NaluPacket::NALU_SPS)
	{
		pktOut.flags |= MEDIA_FLAG_KEY;
	}
	else if (parserPkt.type == NaluPacket::NALU_PPS)
	{
		pktOut.flags |= MEDIA_FLAG_KEY;
	}
	else if (parserPkt.type == NaluPacket::NALU_IFRAME)
	{
		pktOut.flags |= MEDIA_FLAG_KEY;
	}

	return true;
}

void PSRtpPackager::reset()
{
	/// pass
}




} /* namespace av */
