/*    file: RtpHeader.h
 *    desc:
 *   
 * created: 2013-10-16 07:35:44
 *  author: zhengchuanjiang
 * version: 1.0
 * company: 
 */ 


#if !defined RTPHEADER_ZHENGCHUANJIANG_
#define RTPHEADER_ZHENGCHUANJIANG_

////////////////////////////////////////////////////////////////////////////

#define RTP_MAX_SIZE       1160


enum
{
	FU_A = 28
};

#pragma pack(1)

typedef struct 
{
    /* byte 0 */
    unsigned char csrc_len:4; /* CC expect 0 */
    unsigned char extension:1;/* X  expect 1, see RTP_OP below */
    unsigned char padding:1;  /* P  expect 0 */
    unsigned char version:2;  /* V  expect 2 */
    /* byte 1 */
    unsigned char payload:7; /* PT  RTP_PAYLOAD_RTSP */
    unsigned char marker:1;  /* M   expect 1 */
    /* byte 2,3 */
    unsigned short seq_no;   /*sequence number*/
    /* byte 4-7 */
    unsigned  int timestamp;
    /* byte 8-11 */
    unsigned int ssrc; /* stream number is used here. */
} RTP_FIXED_HEADER;/*12 bytes*/

typedef struct {
    unsigned short profile;
    unsigned short length;
} RTP_EXT_HEADER;

#pragma pack()

/******************************************************************
NALU_HEADER
+---------------+
|0|1|2|3|4|5|6|7|
+-+-+-+-+-+-+-+-+
|F|NRI|  Type   |
+---------------+
******************************************************************/
typedef struct {
    //byte 0
    unsigned char TYPE:5;
    unsigned char NRI:2;
    unsigned char F:1;
} NALU_HEADER; /* 1 byte */

/******************************************************************
FU_INDICATOR
+---------------+
|0|1|2|3|4|5|6|7|
+-+-+-+-+-+-+-+-+
|F|NRI|  Type   |
+---------------+
******************************************************************/
typedef struct {
    //byte 0
    unsigned char TYPE:5;
    unsigned char NRI:2; 
    unsigned char F:1;         
} FU_INDICATOR; /*1 byte */

/******************************************************************
FU_HEADER
+---------------+
|0|1|2|3|4|5|6|7|
+-+-+-+-+-+-+-+-+
|S|E|R|  Type   |
+---------------+
******************************************************************/
typedef struct {
    //byte 0
    unsigned char TYPE:5;
    unsigned char R:1;
    unsigned char E:1;
    unsigned char S:1;
} FU_HEADER; /* 1 byte */

////////////////////////////////////////////////////////////////////////////
#endif //RTPHEADER_ZHENGCHUANJIANG_

