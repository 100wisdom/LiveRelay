/*
 * RtpSink.h
 *
 *  Created on: 2016年4月29日
 *      Author: terry
 */

#ifndef RTPSINK_H_
#define RTPSINK_H_

#include "BasicType.h"
#include "RtpCaster.h"
#include "SharedPtr.h"

namespace av
{


class RtpSink
{
public:
	virtual ~RtpSink() {}

	virtual int open(int port) =0;

	virtual void close() =0;

	virtual bool isOpen() =0;

	virtual void setCallback(RtpSinkCallback cb, void* context) =0;

	virtual void setFormat(int codec, int clockRate) =0;

	virtual void setCacheSize(int size) =0;

	virtual int getPort() =0;

};


typedef std::shared_ptr< RtpSink >		RtpSinkPtr;

}



#endif /* RTPSINK_H_ */
