/*
 * Application.cpp
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#include "Application.h"
#include "Socket.h"

typedef comn::Singleton< Application >		ApplicationSingleton;


Application& Application::instance()
{
	return ApplicationSingleton::instance();
}

Application::Application()
{
}

Application::~Application()
{
}


int Application::init()
{
	comn::Socket::startup();

	return 0;
}

void Application::quit()
{
	m_chlTable.clear();

	comn::Socket::cleanup();
}

av::CasterChannelTable& Application::getChannelTable()
{
	return m_chlTable;
}

av::RtpSinkTable& Application::getSinkTable()
{
	return av::RtpSinkTable::instance();
}
