/*
 * JRTPSession.cpp
 *
 *  Created: 2011-10-19
 *   Author: terry
 */

#include "JRTPSession.h"


JRTPSession::JRTPSession(RTPMemoryManager *mgr):
        RTPSession(NULL, mgr),
        m_isRtcpTimeout(false)
{
}


void JRTPSession::OnNewSource(RTPSourceData *dat)
{
    if (dat->IsOwnSSRC())
    {
        return;
    }

    unsigned int  ip;
    unsigned short  port;
    if (dat->GetRTPDataAddress() != 0)
    {
        const RTPIPv4Address *addr = (const RTPIPv4Address *)(dat->GetRTPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort();
    }
    else if (dat->GetRTCPDataAddress() != 0)
    {
        const RTPIPv4Address *addr = (const RTPIPv4Address *)(dat->GetRTCPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort()-1;
    }
    else
    {
        return;
    }

    RTPIPv4Address dest(ip,port);
    AddDestination(dest);
    return;
}

void JRTPSession::OnBYEPacket(RTPSourceData *dat)
{
    if (dat->IsOwnSSRC())
    {
        return;
    }

    unsigned int  ip;
    unsigned short  port;
    if (dat->GetRTPDataAddress() != 0)
    {
        const RTPIPv4Address *addr =
            (const RTPIPv4Address *)(dat->GetRTPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort();
    }
    else if (dat->GetRTCPDataAddress() != 0)
    {
        const RTPIPv4Address *addr =
            (const RTPIPv4Address *)(dat->GetRTCPDataAddress());
        ip = addr->GetIP();
        port = addr->GetPort()-1;
    }
    else
    {
        return;
    }

    RTPIPv4Address dest(ip,port);
    DeleteDestination(dest);

    return;

}

void JRTPSession::OnRemoveSource(RTPSourceData *dat)
{
    if (dat->IsOwnSSRC())
    {
        return;
    }
    if (dat->ReceivedBYE())
    {
        return;
    }

    return;
}


void JRTPSession::OnTimeout(RTPSourceData *srcdat)
{
    m_isRtcpTimeout = true;

}

bool JRTPSession::isRtcpTimeout() const
{
    return m_isRtcpTimeout;
}
