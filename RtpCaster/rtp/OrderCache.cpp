/*    file: OrderCache.cpp
 *    desc:
 * 
 * created: 2013-09-10 09:47:37
 *  author: zhengchuanjiang
 * company: 
 */

#include "OrderCache.h"



OrderCache::OrderCache(size_t fullSize):
m_fullSize(fullSize)
{
}

OrderCache::~OrderCache()
{

}

void OrderCache::push(RTPPacketPtr pPacket)
{
    m_packets.insert(pPacket);
}

size_t OrderCache::size() const
{
    return m_packets.size();
}

bool OrderCache::empty() const
{
    return m_packets.empty();
}

RTPPacketPtr OrderCache::pop()
{
    RTPPacketPtr pPacket = NULL;
    if (!m_packets.empty())
    {
        pPacket = m_packets.front();
        m_packets.pop_front();
    }
    return pPacket;
}

void OrderCache::setFullSize(size_t fullSize)
{
    m_fullSize = fullSize;
}

size_t OrderCache::fullSize() const
{
    return m_fullSize;
}

bool OrderCache::isReady() const
{
    return (size() >= m_fullSize);
}



