/*    file: RtpPacketPtr.cpp
 *    desc:
 * 
 * created: 2013-09-10 11:16:20
 *  author: zhengchuanjiang
 * company: 
 */

#include "Jrtplib.h"
#include "RtpPacketPtr.h"
#include "CLog.h"

bool RtpPacketOrder::operator ()(const RTPPacketPtr& l, const RTPPacketPtr& r) const
{
    bool order = (l->GetExtendedSequenceNumber() < r->GetExtendedSequenceNumber());
    return order;
}


