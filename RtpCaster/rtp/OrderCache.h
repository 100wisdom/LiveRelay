/*    file: OrderCache.h
 *    desc:
 *   
 * created: 2013-09-10 09:47:36
 *  author: zhengchuanjiang
 * company: 
 */ 


#if !defined ORDERCACHE_ZHENGCHUANJIANG_
#define ORDERCACHE_ZHENGCHUANJIANG_

#include "RtpPacketPtr.h"
#include "TSortedList.h"
////////////////////////////////////////////////////////////////////////////

/**
 * rtp包排序缓冲区
 */
class OrderCache
{
public:
    enum Constant
    {
        DEFAULT_FULL_SIZE = 10
    };

    typedef comn::SortedList< RTPPacketPtr, RtpPacketOrder >    PacketList;

public:
    explicit OrderCache(size_t fullSize=DEFAULT_FULL_SIZE);
    ~OrderCache();

    void push(RTPPacketPtr pPacket);

    size_t size() const;

    bool empty() const;

    RTPPacketPtr pop();

    void setFullSize(size_t fullSize);

    size_t fullSize() const;

    bool isReady() const;

protected:
    PacketList  m_packets;
    size_t  m_fullSize;

};

////////////////////////////////////////////////////////////////////////////
#endif //ORDERCACHE_ZHENGCHUANJIANG_

