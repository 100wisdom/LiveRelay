/*
 * NetAddress.cpp
 *
 *  Created on: 2015年6月27日
 *      Author: chuanjiang.zh@qq.com
 */

#include "NetAddress.h"

namespace av
{

NetAddress::NetAddress():
		m_ip(),
		m_port()
{
}

NetAddress::~NetAddress()
{
}

NetAddress::NetAddress(const std::string& ip, uint16_t port):
		m_ip(ip),
		m_port(port)
{

}

void NetAddress::set(const std::string& ip, uint16_t port)
{
    m_ip = ip;
    m_port = port;
}

NetAddress::NetAddress(const NetAddress& obj):
		m_ip(obj.m_ip),
		m_port(obj.m_port)
{
}

NetAddress& NetAddress::operator = (const NetAddress& obj)
{
	m_ip = obj.m_ip;
	m_port = obj.m_port;
	return *this;
}

bool NetAddress::operator == (const NetAddress& other)
{
	return (m_ip == other.m_ip) && (m_port == other.m_port);
}




} /* namespace av */
