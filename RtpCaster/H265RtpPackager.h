/*
 * H265RtpPackager.h
 *
 *  Created on: 2016年4月23日
 *      Author: terry
 */

#ifndef H265RTPPACKAGER_H_
#define H265RTPPACKAGER_H_

#include "RtpPackager.h"
#include "TByteBuffer.h"


namespace av
{

class H265RtpPackager: public RtpPackager
{
public:
	H265RtpPackager();
	virtual ~H265RtpPackager();

    virtual void slice(const MediaPacket& pkt, int maxSize, RtpPackagerSink* pSink);

    virtual bool join(const RtpPacket& pktIn, MediaPacket& pktOut);

    virtual void reset();

protected:
    void sliceNaluPtr(uint8_t* data, uint8_t* end, uint32_t timestamp, bool mark,
            int maxSize, RtpPackagerSink* pSink);

    void sliceNalu(uint8_t* data, int size, uint32_t timestamp, bool mark,
        int maxSize, RtpPackagerSink* pSink);

    comn::ByteBuffer   m_sliceBuffer;
    comn::ByteBuffer   m_joinBuffer;
    uint32_t    m_lastTime;

};

} /* namespace av */

#endif /* H265RTPPACKAGER_H_ */
