/*    file: H264NaluType.h
 *    desc:
 *   
 * created: 2014-06-22 21:24:08
 *  author: zhengchuanjiang
 * version: 1.0
 * company: 
 */ 


#if !defined H264NALUTYPE_H_
#define H264NALUTYPE_H_

////////////////////////////////////////////////////////////////////////////
enum H264NaluType
{
    kNaluNone = 0,
    kNaluSPS = 7,
    kNaluPPS = 8,
    kNaluSEI = 6,
    kNaluIDR = 5
};

////////////////////////////////////////////////////////////////////////////
#endif //H264NALUTYPE_H_

