/*
 * CasterChannel.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef CASTERCHANNEL_H_
#define CASTERCHANNEL_H_

#include "MediaStream.h"


namespace av
{

class CasterChannel
{
public:
	virtual ~CasterChannel() {}

	virtual int open(int codec, int clockRate, int outPort, int ssrc) =0;

	virtual void close() =0;

	virtual bool isOpen() =0;

	virtual int write(MediaPacketPtr& pkt) =0;

	virtual int addTarget(int protocol, const char* ip, int port) =0;

	virtual int removeTarget(int protocol, const char* ip, int port) =0;

	virtual int removeAllTarget(int protocol) =0;

};


typedef std::shared_ptr< CasterChannel >		CasterChannelPtr;



} /* namespace av */

#endif /* CASTERCHANNEL_H_ */
