﻿/*    file: CasterDelay.h
 *    desc:
 *   
 * created: 2015-11-16
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined CASTERDELAY_H_
#define CASTERDELAY_H_

#include "BasicType.h"
////////////////////////////////////////////////////////////////////////////

/**
 * 发送延时计算器
 */
class CasterDelay
{
public:
    enum Constant
    {
        kMinDelay = 0,
        kMaxDelay = 30,
        kQueueFullSize = 3, /// 缓存满, 降低延时发送
        kQueueMaxSize = 10, /// 缓存帧最大数
        
    };

public:
    CasterDelay();
    ~CasterDelay();

    /**
     * 根据发送缓冲大小,计算延时比例
     * @param queueSize	发送缓冲区大小
     * @return	延时比例,取值范围为[0,1]
     */
    double computeDelay(size_t queueSize);

    /**
     * 限制延时的绝对值取值范围
     * @param delay
     * @return 限制后的延时值
     */
    int32_t limitDelay(int32_t delay);

};
////////////////////////////////////////////////////////////////////////////
#endif //CASTERDELAY_H_

