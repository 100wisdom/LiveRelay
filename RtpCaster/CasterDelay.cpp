/*    file: CasterDelay.cpp
 *    desc:
 * 
 * created: 2015-11-16
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include "CasterDelay.h"

CasterDelay::CasterDelay()
{

}

CasterDelay::~CasterDelay()
{

}

double CasterDelay::computeDelay(size_t queueSize)
{
    if (queueSize > kQueueMaxSize)
    {
        return 0.0;
    }
    else if (queueSize < kQueueFullSize)
    {
        return 1.0;
    }
    return ((double)(queueSize - kQueueFullSize)) / (kQueueMaxSize - kQueueFullSize);
}

int32_t CasterDelay::limitDelay(int32_t delay)
{
    if (delay < kMinDelay)
    {
        return kMinDelay;
    }
    else if (delay > kMaxDelay)
    {
        delay = kMaxDelay;
    }
    return delay;
}
