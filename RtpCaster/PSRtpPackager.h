/*
 * PSRtpPackager.h
 *
 *  Created on: 2015年7月5日
 *      Author: terry
 */

#ifndef PSRTPPACKAGER_H_
#define PSRTPPACKAGER_H_

#include "RtpPackager.h"
#include "ProgramStreamParser.h"


namespace av
{

/**
 * PS RTP包打包器
 */
class PSRtpPackager: public RtpPackager
{
public:
	PSRtpPackager();
	virtual ~PSRtpPackager();

    virtual void slice(const MediaPacket& pkt, int maxSize, RtpPackagerSink* pSink);

    virtual bool join(const RtpPacket& pktIn, MediaPacket& pktOut);

    virtual void reset();

private:
    ProgramStreamParser	m_parser;

    uint32_t m_lastPktTime;

};


} /* namespace av */

#endif /* PSRTPPACKAGER_H_ */
