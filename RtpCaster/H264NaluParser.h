/*    file: H264NaluParser.h
 *    desc:
 *   
 * created: 2014-06-16 23:11:28
 *  author: zhengchuanjiang
 * version: 1.0
 * company: 
 */ 


#if !defined H264NALUPARSER_H_
#define H264NALUPARSER_H_

#include "BasicType.h"
#include <string>

////////////////////////////////////////////////////////////////////////////

class NaluPacket
{
public:
    enum NaluType
    {
        NALU_NULL = 0,
        NALU_SPS = 7,
        NALU_PPS = 8,
        NALU_SEI = 6,
        NALU_IFRAME = 5
    };

    const uint8_t*    data;
    int length;
    uint8_t type;
    int prefix;

    NaluPacket():
        data(),
        length(),
        type(),
        prefix()
    {
    }

    ~NaluPacket()
    {

    }
};

class H264NaluParser
{
public:
    H264NaluParser();
    ~H264NaluParser();

    static const size_t START_CODE_LENGTH = 4;
    
    static const uint8_t s_startCode[START_CODE_LENGTH];

    static const uint8_t* getStartCode();

    static bool startWithH264Code(const uint8_t* data, size_t length);

    static size_t findH264StartCode(const uint8_t* data, size_t length, size_t start);

    static void insertStartCode(std::string& data);

    static bool parseNalu(const uint8_t* data, size_t length, NaluPacket& pkt);

    static int parseNaluType(uint8_t data);


};
////////////////////////////////////////////////////////////////////////////
#endif //H264NALUPARSER_H_

