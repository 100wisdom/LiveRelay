/*
 * RtpMediaCaster.h
 *
 *  Created on: 2016年4月23日
 *      Author: terry
 */

#ifndef RTPMEDIACASTER_H_
#define RTPMEDIACASTER_H_

#include "MediaStream.h"

namespace av
{

class RtpMediaCaster : public MediaSink
{
public:
	virtual ~RtpMediaCaster() {}

	virtual int open(const std::string& ip, uint16_t port, int codec, int clockRate, int payload, int ssrc) =0;

	virtual void close() =0;

	virtual bool isOpen() =0;

	virtual void setID(int id) =0;

	virtual int getID() =0;

	virtual bool addTarget(const std::string& ip, uint16_t port) =0;

	virtual void removeTarget(const std::string& ip, uint16_t port) =0;

	virtual size_t getTargetCount() =0;

	virtual void removeAllTarget() =0;

};


typedef std::shared_ptr< RtpMediaCaster >		RtpMediaCasterPtr;



} /* namespace av */

#endif /* RTPMEDIACASTER_H_ */
