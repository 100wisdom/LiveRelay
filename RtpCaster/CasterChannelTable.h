/*
 * CasterChannelTable.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef CASTERCHANNELTABLE_H_
#define CASTERCHANNELTABLE_H_

#include "CasterChannel.h"
#include "TMap.h"


namespace av
{


class CasterChannelTable : public comn::Map< void*, CasterChannelPtr >
{
public:
	static CasterChannelTable& instance();

public:
	CasterChannelTable();
	virtual ~CasterChannelTable();

protected:


};



} /* namespace av */

#endif /* CASTERCHANNELTABLE_H_ */
