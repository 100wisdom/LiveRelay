/*
 * Application.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef APPLICATION_H_
#define APPLICATION_H_

#include "TSingleton.h"
#include "SharedPtr.h"
#include "CasterChannelTable.h"
#include "RtpSinkTable.h"


class Application
{
public:
	static Application& instance();

public:
	Application();
	virtual ~Application();

	int init();

	void quit();

	av::CasterChannelTable& getChannelTable();

	av::RtpSinkTable& getSinkTable();

private:
	av::CasterChannelTable	m_chlTable;
	av::RtpSinkTable	m_sinkTable;

};

#endif /* APPLICATION_H_ */
