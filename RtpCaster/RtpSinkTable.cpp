/*
 * RtpSinkTable.cpp
 *
 *  Created on: 2016年4月29日
 *      Author: terry
 */

#include "RtpSinkTable.h"
#include "TSingleton.h"


namespace av
{

typedef comn::Singleton< RtpSinkTable >		RtpSinkTableSingleton;


RtpSinkTable& RtpSinkTable::instance()
{
	return RtpSinkTableSingleton::instance();
}

RtpSinkTable::RtpSinkTable()
{
}

RtpSinkTable::~RtpSinkTable()
{
}

} /* namespace av */
