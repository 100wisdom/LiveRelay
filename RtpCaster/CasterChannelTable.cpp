/*
 * CasterChannelTable.cpp
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#include "CasterChannelTable.h"
#include <algorithm>
#include "TSingleton.h"


namespace av
{

typedef comn::Singleton< CasterChannelTable >		CasterChannelTableSingleton;


CasterChannelTable& CasterChannelTable::instance()
{
	return CasterChannelTableSingleton::instance();
}


CasterChannelTable::CasterChannelTable()
{
}

CasterChannelTable::~CasterChannelTable()
{
}



} /* namespace av */
