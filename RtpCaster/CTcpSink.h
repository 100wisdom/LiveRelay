/*
 * CTcpSink.h
 *
 *  Created on: 2016年5月13日
 *      Author: terry
 */

#ifndef CTCPSINK_H_
#define CTCPSINK_H_

#include "BasicType.h"
#include "RtpSink.h"
#include "Socket.h"
#include "TThread.h"
#include "RtpPackager.h"
#include "TCriticalSection.h"
#include "TByteBuffer.h"


namespace av
{

typedef std::shared_ptr< RtpPackager >	RtpPackagerPtr;

/**
 * 基于TCP的RTP接收端
 * 作为监听方, 接收连接
 */
class CTcpSink: public RtpSink , public comn::Thread
{
public:
	CTcpSink();
	virtual ~CTcpSink();

	virtual int open(int port);

	virtual void close();

	virtual bool isOpen();

	virtual void setCallback(RtpSinkCallback cb, void* context);

	virtual void setFormat(int codec, int clockRate);

	virtual void setCacheSize(int size);

	virtual int getPort();

protected:
	virtual int run();
	virtual void doStop();

protected:
	bool openSession(int port);
	void closeSession();

	void fireMediaPacket(MediaPacket& packet);

	void handleListen();
	void handleRead();
	void handleClose();

	void onRtpPacket();

protected:
	comn::CriticalSection   m_cs;

	int	m_port;
	comn::Socket	m_listenSocket;
	comn::Socket	m_connSocket;

	int			m_codec;
	uint32_t    m_clockRate;
	int		m_payload;

	RtpSinkCallback m_callback;
	void* m_context;

	RtpPackagerPtr	m_packager;
	comn::ByteBuffer	m_buffer;

};



} /* namespace av */


#endif /* CTCPSINK_H_ */
