/*
 * RtpSinkTable.h
 *
 *  Created on: 2016年4月29日
 *      Author: terry
 */

#ifndef RTPSINKTABLE_H_
#define RTPSINKTABLE_H_

#include "RtpSink.h"
#include "TMap.h"

namespace av
{

class RtpSinkTable : public comn::Map< void*, RtpSinkPtr >
{
public:
	static RtpSinkTable& instance();

public:
	RtpSinkTable();
	virtual ~RtpSinkTable();
};

} /* namespace av */

#endif /* RTPSINKTABLE_H_ */
