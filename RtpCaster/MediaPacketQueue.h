/*
 * MediaPacketQueue.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef MEDIAPACKETQUEUE_H_
#define MEDIAPACKETQUEUE_H_

#include "MediaPacket.h"
#include <deque>
#include "TCriticalSection.h"
#include "TEvent.h"


namespace av
{

class MediaPacketQueue
{
public:
	MediaPacketQueue();
	virtual ~MediaPacketQueue();

	size_t size();

	bool empty();

	size_t push(MediaPacketPtr& packet);

	MediaPacketPtr pop();

	bool pop(MediaPacketPtr& packet);

	void clear();

	/**
	 * 在指定时间内等待媒体包
	 * 如果队列不空,立即返回true
	 * @param ms	毫秒
	 * @return true 表示队列有元素了
	 */
	bool timedwait(int ms);

	/**
	 * 如果队列为空, 等待, 并弹出头部元素
	 * @param packet
	 * @param ms
	 * @return
	 */
	bool pop(MediaPacketPtr& packet, int ms);

	/// 取消等待
	void cancelWait();

protected:
    typedef std::deque< MediaPacketPtr >  PacketDeque;

protected:
    PacketDeque m_packets;
    comn::CriticalSection   m_cs;
    comn::Event	m_event;


};



} /* namespace av */

#endif /* MEDIAPACKETQUEUE_H_ */
