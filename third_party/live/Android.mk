LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := liveMedia
LOCAL_MODULE_FILENAME := libliveMedia

LOCAL_CPPFLAGS += -fexceptions -frtti -DXLOCALE_NOT_USED=1

LOCAL_C_INCLUDES := $(LOCAL_PATH)/ \
	$(LOCAL_PATH)/BasicUsageEnvironment/include \
	$(LOCAL_PATH)/groupsock/include \
	$(LOCAL_PATH)/liveMedia/include \
	$(LOCAL_PATH)/UsageEnvironment/include \

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_C_INCLUDES)

SRC_FILES := $(wildcard \
		$(LOCAL_PATH)/groupsock/*.c \
		$(LOCAL_PATH)/groupsock/*.cpp \
		$(LOCAL_PATH)/UsageEnvironment/*.cpp \
		$(LOCAL_PATH)/BasicUsageEnvironment/*.cpp \
		$(LOCAL_PATH)/liveMedia/*.cpp \
		$(LOCAL_PATH)/liveMedia/*.c \
		)
		
SRC_FILES := $(SRC_FILES:$(LOCAL_PATH)/%=%)

LOCAL_SRC_FILES += $(SRC_FILES)

#LOCAL_CFLAGS += -D

include $(BUILD_STATIC_LIBRARY)

