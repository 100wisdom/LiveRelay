#!/bin/sh


cd jrtplib-3.8.2
autoconf 
./configure
make all
cp src/.libs/libjrtp.a          ../lib/
cp src/.libs/libjrtp-3.8.2.so   ../lib/
cd ..

cd jthread-1.2.1
autoconf
./configure
make all
cp src/.libs/libjthread.a  ../lib/
cp src/.libs/libjthread-1.2.1.so ../lib/
cd ..

