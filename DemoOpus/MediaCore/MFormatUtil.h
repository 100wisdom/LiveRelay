/*
 * MFormatUtil.h
 *
 *  Created on: 2017年3月6日
 *      Author: chuanjiang.zh
 */

#ifndef MFORMATUTIL_H_
#define MFORMATUTIL_H_

#include "BasicType.h"
#include "MFormat.h"
#include <string>
#include "MediaFormat.h"


namespace av
{


class MFormatUtil
{
public:
	MFormatUtil();
	virtual ~MFormatUtil();

	static void free(MFormat& fmt);

	static void clone(const MFormat& srcFmt, MFormat& fmt);

	static void setVideoProp(MFormat& fmt, const uint8_t* data, size_t length);

	static void setVideoProp(MFormat& fmt, const std::string& prop);

	static void copyTo(const MFormat* fmt, av::MediaFormat& mfmt);

	static void copyFrom(MFormat* fmt, const av::MediaFormat& mfmt);

	static std::string toString(const MFormat& fmt);

	static void copy(const MFormat& srcFmt, MFormat& fmt);
	static void copyFull(const MFormat& srcFmt, MFormat& fmt);
	static void copyVideo(const MFormat& srcFmt, MFormat& fmt);
	static void copyAudio(const MFormat& srcFmt, MFormat& fmt);
	static bool check(const MFormat& fmt);

};



} /* namespace av */

#endif /* MFORMATUTIL_H_ */
