/*
 * TimeHelper.h
 *
 *  Created on: 2015年12月31日
 *      Author: terry
 */

#ifndef UTIL_TIMEHELPER_H_
#define UTIL_TIMEHELPER_H_

#include "BasicType.h"
#include <time.h>

namespace util
{

class TimeHelper
{
public:
	TimeHelper();
	virtual ~TimeHelper();

	static int64_t getTime();

	/// monotonic clock
	static int64_t getClock();

};

} /* namespace util */

#endif /* UTIL_TIMEHELPER_H_ */
