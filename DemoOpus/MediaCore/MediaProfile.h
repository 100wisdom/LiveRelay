/*
 * MediaProfile.h
 *
 *  Created on: 2016年1月12日
 *      Author: terry
 */

#ifndef MEDIAPROFILE_H_
#define MEDIAPROFILE_H_

#include "MediaType.h"

namespace av
{


class VideoCodecProfile
{
public:
	int codec;
	int bitrate;
	int gop;	/// key frame interval
	int profile;
	int level;

};


class AudioCodecProfile
{
public:
	int codec;
	int bitrate;
	int profile;
	int level;
};



class MediaProfile
{
public:
	int m_codec;	///
	int	m_width;
	int m_height;
	int m_fps;
	int m_bitrate;
	int m_gop;	/// key frame interval
	int m_profile;
	int m_level;

	int m_audioCodec;
	int m_channels;
	int m_samplerate;
    int m_sampleFormat;
	int m_audioBitrate;
	int m_audioProfile;
	int m_audioLevel;

	MediaProfile():
		m_codec(),
		m_width(),
		m_height(),
		m_fps(),
		m_bitrate(),
		m_gop(),
		m_profile(),
		m_level(),
		m_audioCodec(),
		m_channels(),
		m_samplerate(),
		m_sampleFormat(),
		m_audioBitrate(),
		m_audioProfile(),
		m_audioLevel()
	{
	}

	bool isValidResolution() const
	{
		return (m_width > 0) && ((m_width % 4) == 0) &&
				(m_height > 0) && ((m_height % 4 == 0));
	}

	bool checkAudio() const
	{
		return (m_channels > 0) && (m_samplerate > 0);
	}


};



}


#endif /* MEDIAPROFILE_H_ */
