/*
 * AudioFrameReader.h
 *
 *  Created on: 2019年1月15日
 *      Author: terry
 */

#ifndef AUDIOFRAMEREADER_H_
#define AUDIOFRAMEREADER_H_

#include "BasicType.h"
#include <string>

#include "Ffmpeg.h"


namespace av
{

class AudioFrameReader
{
public:
    virtual ~AudioFrameReader() {}

    virtual bool open(const std::string& url, const std::string& params, int channels, int sampleRate, AVSampleFormat fmt) =0;

    virtual void close() =0;

    virtual bool isOpen() =0;

    virtual int read(AVFrame* frame) =0;

    virtual bool getFormat(int& channels, int& sampleRate, AVSampleFormat& fmt) =0;

};


} /* namespace av */

#endif /* AUDIOFRAMEREADER_H_ */
