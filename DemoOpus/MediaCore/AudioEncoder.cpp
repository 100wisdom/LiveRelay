/*
 * AudioEncoder.cpp
 *
 *  Created on: 2017年9月29日
 *      Author: terry
 */

#include "AudioEncoder.h"
#include "Ffmpeg.h"
#include "CLog.h"
#include "CAudioEncoder.h"

#ifdef ARM_LINUX
	#include "FdkAudioEncoder.h"
#endif //


static void setupLog()
{
    CLog::setLogger(CLog::COUT, CLog::kNone, 0);

    CLog::setLogger(CLog::DEBUGWINDOW, CLog::kNone, 0);
}

void AudioEncoderFactory::startup()
{
	setupLog();

	av_register_all();
}

void AudioEncoderFactory::cleanup()
{
	avformat_network_deinit();
}

AudioEncoder* AudioEncoderFactory::create(const char* params)
{
// #ifdef ARM_LINUX
	// if (strlen(params) == 0 || (strcmp(params, "fdk") == 0))
	// {
		// return new FdkAudioEncoder();
	// }
	// return new CAudioEncoder();
// #else
	return new CAudioEncoder();
// #endif //
}

void AudioEncoderFactory::destroy(AudioEncoder* encoder)
{
	if (encoder)
	{
		encoder->close();
		delete encoder;
	}
}

