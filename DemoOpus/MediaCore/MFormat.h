/*
 * MFormat.h
 *
 *  Created on: 2017年3月1日
 *      Author: chuanjiang.zh
 */

#ifndef MFORMAT_H_
#define MFORMAT_H_

#ifdef _MSC_VER
typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef short int16_t;
typedef unsigned short uint16_t;
typedef int int32_t;
typedef unsigned uint32_t;
typedef long long int64_t;
typedef unsigned long long uint64_t;
#else
#include <stdint.h>
typedef void *HANDLE;
#endif //_MSC_VER

#ifndef MKBETAG
#define MKBETAG(a,b,c,d) ((d) | ((c) << 8) | ((b) << 16) | ((unsigned)(a) << 24))
#endif //MKBETAG


#ifndef CASTER_TYPE
#define	CASTER_TYPE


/// 编码.
enum MCodec
{
	MCODEC_NONE = 0,

	MCODEC_H264 = 28,
	MCODEC_HEVC = 174, /// H.265
	MCODEC_H265 = MCODEC_HEVC,

	MCODEC_G711U = 65542,
	MCODEC_G711A,

	MCODEC_MP3 = 0x15001,
	MCODEC_AAC = 0x15002,
	MCODEC_AC3 = 0x15003,
	MCODEC_VORBIS = 0x15005,

	MCODEC_RAW = 0x10101010

};

enum MType
{
	MTYPE_NONE = -1,
	MTYPE_VIDEO = 0,
	MTYPE_AUDIO,
	MTYPE_DATA,
};


/// 媒体格式. 
struct MFormat
{
	int codec;		/// 视频编码  @see MCodec
	int width;		/// 视频高. 
	int height;		/// 视频宽. 
	int framerate;		/// 帧率. 
	int profile;
	int clockRate;  /// 时钟频率. 

	int audioCodec;	/// 音频编码  @see MCodec
	int channels;	/// 通道数. 
	int sampleRate;	/// 采样率. 
	int audioProfile;	/// 档次. 
	int audioRate;      /// 音频时钟频率. 

	int vPropSize;		/// 视频解码参数, 对于H.264是sps+pps, 对于H.265是vps+sps+pps
	unsigned char* vProp;

	int configSize;		/// 音频解码参数, 如果是AAC编码, 则必须设置为AAC的config参数.
	unsigned char* config;

    int bitrate;    ///码率. 
    int audioBitrate;
};

enum MPacketFlag
{
	MPACKET_FLAG_KEY = 0x01
};

/// 媒体包.
struct MPacket
{
	int type;       ///
	uint8_t* data;	/// 数据指针.
	int size;		/// 数据长度.
	int64_t pts;	/// 时间戳.
	int duration;	/// 时长.
	int flags;		/// 标识.
};

#endif //CASTER_TYPE


#endif /* MFORMAT_H_ */
