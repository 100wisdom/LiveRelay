/*
 * TimeHelper.cpp
 *
 *  Created on: 2015年12月31日
 *      Author: terry
 */

#include "TimeHelper.h"
#include "Ffmpeg.h"


namespace util
{

TimeHelper::TimeHelper()
{
}

TimeHelper::~TimeHelper()
{
}

int64_t TimeHelper::getTime()
{
	return av_gettime();
}

int64_t TimeHelper::getClock()
{
	return av_gettime_relative();
}




} /* namespace util */
