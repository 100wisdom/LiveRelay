/*
 * CAudioFrameSource.h
 *
 *  Created on: 2019年1月15日
 *      Author: terry
 */

#ifndef CAUDIOFRAMESOURCE_H_
#define CAUDIOFRAMESOURCE_H_

#include "AudioFrameSource.h"
#include "AudioFrameReader.h"
#include "SharedPtr.h"

#include "TThread.h"
#include "TEvent.h"

#include "TimePoint.h"


namespace av
{

class CAudioFrameSource : public AudioFrameSource, public comn::Thread
{
public:
    CAudioFrameSource();
    virtual ~CAudioFrameSource();

    virtual void setSink(AVFrameSink* sink);

    virtual bool open(const std::string& url, const std::string& params, int channels, int sampleRate, AVSampleFormat fmt);

    virtual void close();

    virtual bool isOpen();

    virtual bool getFormat(int& channels, int& sampleRate, AVSampleFormat& fmt);

protected:
    virtual int run();

    virtual void doStop();

protected:
    void fireFrame(AVFrame* frame);

protected:
    std::shared_ptr< AudioFrameReader > m_reader;
    AVFrameSink*    m_sink;

    std::string m_url;
    std::string m_params;
    int m_channels;
    int m_sampleRate;
    AVSampleFormat m_sampleFormat;

    comn::Event m_event;

    int64_t m_offsetPts;
    int64_t m_lastPts;

    TimePoint   m_timePoint;

};

} /* namespace av */

#endif /* CAUDIOFRAMESOURCE_H_ */
