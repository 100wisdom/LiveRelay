/*
 * AVPacketPtr.h
 *
 *  Created on: 2017年3月15日
 *      Author: chuanjiang.zh
 */

#ifndef AVPACKETPTR_H_
#define AVPACKETPTR_H_

#include "MediaFormat.h"
#include "Ffmpeg.h"
#include "SharedPtr.h"

namespace av
{

typedef std::shared_ptr< AVPacket >		AVPacketPtr;

inline AVPacketPtr createAVPacket()
{
	AVPacket* pkt = new AVPacket();
	av_init_packet(pkt);
	return AVPacketPtr(pkt, av_packet_unref);
}

}

#endif /* AVPACKETPTR_H_ */
