/*
 * AVFramePtr.h
 *
 *  Created on: 2015年12月23日
 *      Author: terry
 */

#ifndef AVFRAMEPTR_H_
#define AVFRAMEPTR_H_

#include "Ffmpeg.h"
#include "SharedPtr.h"

namespace av
{


inline static void avframe_free(AVFrame* frame)
{
    if (frame)
    {
        av_frame_free(&frame);
    }
}

typedef std::shared_ptr< AVFrame >     AVFramePtr;


inline AVFramePtr createAVFrame()
{
	return AVFramePtr(av_frame_alloc(), avframe_free);
}


}

#endif /* AVFRAMEPTR_H_ */
