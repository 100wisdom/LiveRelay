/*
 * CAudioFrameReader.h
 *
 *  Created on: 2019年1月15日
 *      Author: terry
 */

#ifndef CAUDIOFRAMEREADER_H_
#define CAUDIOFRAMEREADER_H_

#include "AudioFrameReader.h"

#include "TFileWriter.h"

namespace av
{

class CAudioFrameReader : public AudioFrameReader
{
public:
    CAudioFrameReader();
    virtual ~CAudioFrameReader();

    virtual bool open(const std::string& url, const std::string& params, int channels, int sampleRate, AVSampleFormat fmt);

    virtual void close();

    virtual bool isOpen();

    virtual int read(AVFrame* frame);

    virtual bool getFormat(int& channels, int& sampleRate, AVSampleFormat& fmt);

protected:
    bool openFile(const std::string& url, const std::string& params);
    int resample(AVFrame* inFrame, AVFrame* outFrame);

    int readFrame(AVFrame* frame);

protected:
    std::string m_url;
    std::string m_params;

    int m_channels;
    int m_sampleRate;
    AVSampleFormat m_fmt;

    AVFormatContext*    m_fmtContext;
    SwrContext*     m_swrContext;
    int m_audioIndex;


};


} /* namespace av */

#endif /* CAUDIOFRAMEREADER_H_ */
