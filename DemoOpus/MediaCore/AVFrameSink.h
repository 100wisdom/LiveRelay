/*
 * AVFrameSink.h
 *
 *  Created on: 2019年1月15日
 *      Author: terry
 */

#ifndef AVFRAMESINK_H_
#define AVFRAMESINK_H_

#include "BasicType.h"
#include "Ffmpeg.h"

namespace av
{

class AVFrameSink
{
public:
    virtual ~AVFrameSink() {}

    virtual void onFrame(AVFrame* frame) =0;

};

}


#endif /* AVFRAMESINK_H_ */
