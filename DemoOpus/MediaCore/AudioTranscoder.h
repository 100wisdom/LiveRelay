/*
 * AudioTranscoder.h
 *
 *  Created on: 2017年3月6日
 *      Author: chuanjiang.zh
 */

#ifndef AUDIOTRANSCODER_H_
#define AUDIOTRANSCODER_H_

#include "MediaFormat.h"
#include "MediaProfile.h"
#include "Ffmpeg.h"

namespace av
{

class AudioTranscoder
{
public:
	virtual ~AudioTranscoder() {}

	virtual bool open(const MediaFormat& fmt, AVCodecID codecID, int channels, int sampleRate) =0;

	virtual void close() =0;

	virtual bool isOpen() =0;

	virtual bool transcode(AVPacket* srcPacket, AVPacket* destPacket) =0;

	virtual bool getMediaFormat(MediaFormat& fmt) =0;


};


} /* namespace av */

#endif /* AUDIOTRANSCODER_H_ */
