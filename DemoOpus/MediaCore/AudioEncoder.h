/*
 * AudioEncoder.h
 *
 *  Created on: 2017年9月29日
 *      Author: terry
 */

#ifndef AUDIOENCODER_H_
#define AUDIOENCODER_H_

#include "BasicType.h"
#include "MFormat.h"


class AudioEncoder
{
public:
	enum Param
	{
		kAdts = 1
	};
public:
	virtual ~AudioEncoder() {}

	virtual int open(int channels, int rate, int codec, int bitrate) =0;

	virtual void close() =0;

	virtual bool isOpen() =0;

	virtual int encode(uint8_t* data, int size, int64_t pts, MPacket* pkt) =0;

	virtual void flush() =0;

	virtual MFormat& getFormat() =0;

	virtual bool setParam(int param, int value) =0;

};


class  AudioEncoderFactory
{

public:
	static void startup();

	static void cleanup();

	/**
	 * 创建编码器.
	 * @return
	 */
	static AudioEncoder* create(const char* params);

	/**
	 * 销毁编码器
	 * @param encoder
	 */
	static void destroy(AudioEncoder* encoder);


	AudioEncoderFactory()
	{
		startup();
	}

	~AudioEncoderFactory()
	{
		cleanup();
	}

};



#endif /* AUDIOENCODER_H_ */
