/*
 * CodecIDConvert.h
 *
 *  Created on: 2018年8月16日
 *      Author: zhengboyuan
 */

#ifndef CODECIDCONVERT_H_
#define CODECIDCONVERT_H_

#include "MediaType.h"
#include "Ffmpeg.h"

namespace av
{

class CodecIDConvert
{
public:
    CodecIDConvert();
    virtual ~CodecIDConvert();

	static AVCodecID toAVCodecID(int codec);

	static MediaCodec fromAVCodecID(AVCodecID codecID);

};


} /* namespace av */

#endif /* CODECIDCONVERT_H_ */
