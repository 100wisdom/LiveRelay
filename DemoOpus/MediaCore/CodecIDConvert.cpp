/*
 * CodecIDConvert.cpp
 *
 *  Created on: 2018年8月16日
 *      Author: zhengboyuan
 */

#include "CodecIDConvert.h"

namespace av
{


struct CodecIDEntry
{
	MediaCodec codec;
	AVCodecID	id;
};

static CodecIDEntry s_codecs[] = 
{
	{ MEDIA_CODEC_NONE , AV_CODEC_ID_NONE},
	{ MEDIA_CODEC_MPEG2, AV_CODEC_ID_MPEG2VIDEO},
	{ MEDIA_CODEC_MPEG4, AV_CODEC_ID_MPEG4},

	{ MEDIA_CODEC_H264, AV_CODEC_ID_H264},
	{ MEDIA_CODEC_VP8, AV_CODEC_ID_VP8},

	{ MEDIA_CODEC_HEVC, AV_CODEC_ID_HEVC },
	
	{ MEDIA_CODEC_THEORA, AV_CODEC_ID_THEORA},
	
	{ MEDIA_CODEC_G711U, AV_CODEC_ID_PCM_MULAW},
	{ MEDIA_CODEC_G711A, AV_CODEC_ID_PCM_ALAW},

	//{ MEDIA_CODEC_G722, AV_CODEC_ID_ADPCM_G722},
	//{ MEDIA_CODEC_G723_1, AV_CODEC_ID_G723_1},
	//{ MEDIA_CODEC_G729, AV_CODEC_ID_G729},
	//{ MEDIA_CODEC_G726, AV_CODEC_ID_ADPCM_G726},
	//{ MEDIA_CODEC_MP2, AV_CODEC_ID_MP2},

	{ MEDIA_CODEC_MP3, AV_CODEC_ID_MP3},
	{ MEDIA_CODEC_AAC, AV_CODEC_ID_AAC},
	{ MEDIA_CODEC_AC3, AV_CODEC_ID_AC3},
	{ MEDIA_CODEC_VORBIS, AV_CODEC_ID_VORBIS},
    { MEDIA_CODEC_OPUS, AV_CODEC_ID_OPUS},

	{ MEDIA_CODEC_RAW, AV_CODEC_ID_RAWVIDEO}
};


CodecIDConvert::CodecIDConvert()
{
}

CodecIDConvert::~CodecIDConvert()
{
}

AVCodecID CodecIDConvert::toAVCodecID(int codec)
{
	size_t count = sizeof(s_codecs) / sizeof(s_codecs[0]);
	for (size_t i = 0; i < count; i++)
	{
		if (codec == s_codecs[i].codec)
		{
			return s_codecs[i].id;
		}
	}
	return (AVCodecID)codec;
}

MediaCodec CodecIDConvert::fromAVCodecID(AVCodecID codecID)
{
	size_t count = sizeof(s_codecs) / sizeof(s_codecs[0]);
	for (size_t i = 0; i < count; i++)
	{
		if (codecID == s_codecs[i].id)
		{
			return s_codecs[i].codec;
		}
	}
	return (MediaCodec)codecID;
}


} /* namespace av */
