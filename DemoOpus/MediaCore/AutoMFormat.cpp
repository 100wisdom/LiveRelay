/*
 * AutoMFormat.cpp
 *
 *  Created on: 2019年1月6日
 *      Author: terry
 */

#include "AutoMFormat.h"
#include "MFormatUtil.h"

using namespace av;


void AutoMFormat::copy(const MFormat& fmt)
{
    MFormatUtil::copyVideo(fmt, *this);
    MFormatUtil::copyAudio(fmt, *this);

    setProp(const_cast<uint8_t*>(fmt.vProp), fmt.vPropSize);
    setConfig(const_cast<uint8_t*>(fmt.config), fmt.configSize);
}

void AutoMFormat::merge(const MFormat& videoFmt, const MFormat& audioFmt)
{
    MFormatUtil::copyVideo(videoFmt, *this);
    MFormatUtil::copyAudio(audioFmt, *this);

    setProp(const_cast<uint8_t*>(videoFmt.vProp), videoFmt.vPropSize);
    setConfig(const_cast<uint8_t*>(audioFmt.config), audioFmt.configSize);
}

void AutoMFormat::setConfig(uint8_t* config, int size)
{
    m_config.clear();
    m_config.write(config, size);

    config = m_config.data();
    configSize = m_config.size();
}

void AutoMFormat::setProp(uint8_t* prop, int size)
{
    m_prop.clear();
    m_prop.write(config, size);

    vProp = m_config.data();
    vPropSize = m_config.size();
}

void AutoMFormat::reserve()
{
    MFormat* fmt = this;

    fmt->vProp = m_prop.data();
    fmt->vPropSize = m_prop.capacity();

    fmt->config = m_config.data();
    fmt->configSize = m_config.capacity();
}
