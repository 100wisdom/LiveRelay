/*
 * CAudioTranscoder.h
 *
 *  Created on: 2017年3月6日
 *      Author: chuanjiang.zh
 */

#ifndef CAUDIOTRANSCODER_H_
#define CAUDIOTRANSCODER_H_

#include "AudioTranscoder.h"
#include <string>
#include "AVFramePtr.h"
#include "FfmpegUtil.h"
#include "AudioEncoder.h"

#include "TFileWriter.h"


namespace av
{

class CAudioTranscoder: public AudioTranscoder
{
public:
	CAudioTranscoder();
	virtual ~CAudioTranscoder();

	virtual bool open(const MediaFormat& fmt, AVCodecID codecID, int channels, int sampleRate);

	virtual void close();

	virtual bool isOpen();

	virtual bool transcode(AVPacket* inPkt, AVPacket* outPkt);

	virtual bool getMediaFormat(MediaFormat& fmt);


protected:
	int openEncoder(AVCodecID codec, int channels, int sampleRate);
	void closeEncoder();
	bool isEncoderOpen();

	bool isSamleFormat(int fmt, int channels, int sampleRate);

	bool getAudioConfig(std::string& config);

	bool transcode(AVFramePtr& frame, AVPacket* outPkt);

	int resample(AVFrame* inFrame, AVFrame* outFrame);

	int openDecoder();
	void closeDecoder();

	bool encode(AVFramePtr& frame, AVPacket* outPkt);

protected:
	MediaFormat	m_fmt;
	AVCodecID m_codecID;
	int m_channels;
	int m_sampleRate;
	int m_channelLayout;

	AVCodecContext*	m_decContext;

	SwrContext*		m_swrContext;
	std::shared_ptr< AudioEncoder >	m_encoder;


};

} /* namespace av */

#endif /* CAUDIOTRANSCODER_H_ */
