/*
 * AutoMFormat.h
 *
 *  Created on: 2019年1月6日
 *      Author: terry
 */

#ifndef AUTOMFORMAT_H_
#define AUTOMFORMAT_H_

#include "BasicType.h"
#include "MFormat.h"
#include "TByteBuffer.h"


class AutoMFormat : public MFormat
{
public:
    AutoMFormat():
        MFormat()
    {
        reset();
    }

    void reset()
    {
        MFormat* fmt = this;
        memset(fmt, 0, sizeof(MFormat));

        m_prop.ensure(1024 * 4);
        fmt->vProp = m_prop.data();
        fmt->vPropSize = m_prop.size();

        m_config.ensure(1024 * 4);
        fmt->config = m_config.data();
        fmt->configSize = m_config.size();
    }

    void copy(const MFormat& fmt);

    void merge(const MFormat& videoFmt, const MFormat& audioFmt);

    size_t capacity() const
    {
        return m_prop.capacity();
    }

    void setConfig(uint8_t* config, int size);

    void setProp(uint8_t* prop, int size);

    void reserve();

    comn::ByteBuffer    m_prop;
    comn::ByteBuffer    m_config;

};

#endif /* AUTOMFORMAT_H_ */
