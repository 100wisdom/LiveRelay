/*
 * AudioFrameSource.h
 *
 *  Created on: 2019年1月15日
 *      Author: terry
 */

#ifndef AUDIOFRAMESOURCE_H_
#define AUDIOFRAMESOURCE_H_

#include "BasicType.h"
#include <string>

#include "Ffmpeg.h"

#include "AVFrameSink.h"


namespace av
{

class AudioFrameSource
{
public:
    virtual ~AudioFrameSource() {}

    virtual void setSink(AVFrameSink* sink) =0;

    virtual bool open(const std::string& url, const std::string& params, int channels, int sampleRate, AVSampleFormat fmt) =0;

    virtual void close() =0;

    virtual bool isOpen() =0;

    virtual bool getFormat(int& channels, int& sampleRate, AVSampleFormat& fmt) =0;


};

} /* namespace av */

#endif /* AUDIOFRAMESOURCE_H_ */
