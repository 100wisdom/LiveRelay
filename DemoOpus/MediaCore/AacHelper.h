/*
 * AacHelper.h
 *
 *  Created on: 2015年12月7日
 *      Author: terry
 */

#ifndef AACHELPER_H_
#define AACHELPER_H_

#include "BasicType.h"
#include <string>

namespace av
{

class AacHelper
{
public:
	static const size_t ADTS_HEADER_SIZE = 7;

public:
	AacHelper();
	virtual ~AacHelper();

	static std::string makeConfig(int profile, int freq, int channels);

	static bool makeAdtsHeader(int profile, int freq, int channels, int packetLen, uint8_t* buf, size_t size);

	static int getFreqIndex(int freq);

	static bool isAdts(uint8_t* buf, size_t size);

	static bool parse(const uint8_t* buf, size_t size, int& profile, int& channels, int& samplerate);

};


} /* namespace av */

#endif /* AACHELPER_H_ */
