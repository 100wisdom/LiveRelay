/*
 * FfmpegUtil.cpp
 *
 *  Created on: 2016-3-14
 *      Author: terry
 */

#include "FfmpegUtil.h"
#include "TStringUtil.h"


namespace ffmpeg
{

void CodecContextDeleter::operator () (AVCodecContext* context)
{
	avcodec_close(context);
	avcodec_free_context(&context);
}

void FormatContextDeleter::operator () (AVFormatContext* context)
{
	avformat_free_context(context);
}

void AVDictionaryDeleter::operator () (AVDictionary* dict)
{
	av_dict_free(&dict);
}

void AVAudioFifoDeleter::operator () (AVAudioFifo* fifo)
{
	av_audio_fifo_free(fifo);
}

void SwrContextDeleter::operator () (SwrContext* context)
{
	swr_free(&context);
}

void AVFrameDeleter::operator () (AVFrame* frame)
{
	av_frame_free(&frame);
}

void PacketDeleter::operator () (AVPacket* pkt)
{
	av_packet_unref(pkt);
    delete pkt;
}

void AVIODeleter::operator () (AVIOContext* context)
{
	avio_close(context);
}



Util::Util()
{

}

Util::~Util()
{
}

bool av_dict_get_int(AVDictionary* m, const char* key, int& value)
{
	bool found = false;
	AVDictionaryEntry* entry = av_dict_get(m, key, NULL, 0);
	if (entry)
	{
		comn::StringCast::toValue(entry->value, value);
	}
	return found;
}

bool av_dict_get_int(AVDictionary* m, const char* key, unsigned int& value)
{
	bool found = false;
	AVDictionaryEntry* entry = av_dict_get(m, key, NULL, 0);
	if (entry)
	{
		comn::StringCast::toValue(entry->value, value);
	}
	return found;
}

bool av_dict_get_int(AVDictionary* m, const char* key, int64_t& value)
{
	bool found = false;
	AVDictionaryEntry* entry = av_dict_get(m, key, NULL, 0);
	if (entry)
	{
		comn::StringCast::toValue(entry->value, value);
	}
	return found;
}



}
