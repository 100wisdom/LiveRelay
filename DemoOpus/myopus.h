#ifndef MYOPUS_H
#define MYOPUS_H
#include <opus_types.h>
#include  <opus.h>
/*OPUS支持的采样率(Hz):8000、12000、16000、24000、48000*/
class myOpus
{
public:
    myOpus(int sampleRate=8000,int channels=1,int samplesize = 16);
    ~myOpus();
    int Pcm2Opus_Enc(unsigned char *pcmdata,int pcmlen,unsigned char *opusdata,int opuslen);
    int Opus2Pcm_Dec(unsigned char *opusdata,int opuslen,unsigned char *pcmdata,int pcmlen);
    char *GetlibOpusVersion();
private:
    OpusEncoder *m_encoder;//编码
    OpusDecoder *m_decoder;//解码
    int m_sampleRate;//采样率
    int m_channels;//声道数
    int m_sampleSize;//位深度
};

#endif // MYOPUS_H
