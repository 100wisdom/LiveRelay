/*    file: main.cpp
 *    desc:
 * 
 * created: 2016-04-06
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include <stdio.h>
#include <iostream>

#include "BasicType.h"
#include "RtspCaster.h"
#include "Ffmpeg.h"
#include "SharedPtr.h"
#include "TFileUtil.h"
#include <assert.h>


extern "C"
{
#include "libavutil/time.h"
}

static bool hasPrefix(const uint8_t* data, int size)
{
    assert(size >= 4);
    if ((data[0] == 0) && (data[1] == 0))
    {
        if ((data[2] == 0) && (data[3] == 1))
        {
            return true;
        }
        if (data[2] == 1)
        {
            return true;
        }
    }
    return false;
}



class Application
{
public:
    Application()
    {
        //caster_init(554);
		//caster_quit();

		caster_init(3554);
        caster_set_event_callback(casterEventCallback, this);
    }

    ~Application()
    {
        caster_quit();
    }

    static void casterEventCallback(const RtspCasterEvent* event, void* context)
    {
        Application* pthis = (Application*)context;
        pthis->casterEventCallback(event);
    }

    void casterEventCallback(const RtspCasterEvent* event)
    {
		/// type = CASTER_SESSION_CREATE 时, name 是带有查询参数的URL路径. 比如 demo?fps=3 . 其他的事件中, name 仅仅是名称.
		/// type = CASTER_CLIENT_DESCRIBE 时, name 是带有查询参数的URL路径.
		/// CASTER_SESSION_CREATE 表示会话创建. 多个客户端共享这个会话(相同名称的会话). 没有客户端连接之后60秒, 会话销毁.
		/// CASTER_CLIENT_DESCRIBE 表示客户端连接进来获取媒体信息事件, 每个客户端都会有该事件.

        printf("caster event. name: %s, type: %d\n", event->name, event->type);
    }

};


int main(int argc, char** argv)
{
	av_register_all();
	avformat_network_init();
	
	if (argc <= 1)
    {
        return EINVAL;
    }

    char* filename = argv[1];
    AVFormatContext* fmtContext = 0;
    int rc = avformat_open_input(&fmtContext, filename, NULL, NULL);
    if (rc != 0)
    {
        return EBADF;
    }

    rc = avformat_find_stream_info(fmtContext, NULL);

    Application app;


    MFormat fmt;
    memset(&fmt, 0, sizeof(fmt));
    
    int videoIndex = -1;
    AVCodecContext* vContext = 0;
    AVCodec* videoCodec = NULL;
    {
        int idx  = av_find_best_stream(fmtContext, AVMEDIA_TYPE_VIDEO, -1, -1, &videoCodec, 0);
        if (videoCodec)
        {
            AVStream* stream = fmtContext->streams[idx];
            vContext = stream->codec;
            fmt.width = vContext->width;
            fmt.height = vContext->height;
            fmt.codec = vContext->codec_id;
            fmt.profile = vContext->profile;

            fmt.vPropSize = vContext->extradata_size;
            fmt.vProp = vContext->extradata;
            fmt.clockRate = 90000;

            if (stream->avg_frame_rate.den > 0)
            {
                fmt.framerate = stream->avg_frame_rate.num / stream->avg_frame_rate.den;
            }
            else if (stream->r_frame_rate.den > 0)
            {
                fmt.framerate = stream->r_frame_rate.num / stream->r_frame_rate.den;
            }
            else 
            {
                fmt.framerate = 25;
            }

            videoIndex = idx;

        }

    }
    
    int audioIndex = -1;
    AVCodecContext* aContext = 0;
    AVCodec* audioCodec = NULL;
    {
        int idx = av_find_best_stream(fmtContext, AVMEDIA_TYPE_AUDIO, -1, -1, &audioCodec, 0);
        if (audioCodec)
        {
            AVStream* stream = fmtContext->streams[idx];
            aContext = stream->codec;

            fmt.audioCodec = aContext->codec_id;
            fmt.sampleRate = aContext->sample_rate;
            fmt.channels = aContext->channels;
            //fmt.audioProfile = aContext->profile;

            fmt.sampleRate = stream->time_base.den;
            fmt.audioRate = stream->time_base.den;

			fmt.configSize = aContext->extradata_size;
			fmt.config = aContext->extradata;

			//static const char* cfg = "aaa";
			//fmt.configSize = 2;
			//fmt.config = (unsigned char*)cfg;

            audioIndex = idx;
        }
    }
    
    HANDLE handle = NULL;
    rc = caster_chl_open(&handle, "demo", &fmt);

	MFormat outFmt;
	memset(&outFmt, 0, sizeof(outFmt));
	rc = caster_chl_get_format(handle, &outFmt);

    AVRational videobase = av_make_q(1, 90000);

    AVPacket pkt;
    av_init_packet(&pkt);

    int64_t tmStart = av_gettime_relative();
    int64_t videoDuration = 0;
    int64_t audioDuration = 0;
    int printCount = 0;

    while (true)
    {
        rc = av_read_frame(fmtContext, &pkt);
        if (rc != 0)
        {
            av_seek_frame(fmtContext, -1, 0, 0);
            std::cin.get();
            continue;
        }

        int delay = 0;
        if (pkt.stream_index == videoIndex)
        {
            AVStream* stream = fmtContext->streams[pkt.stream_index];
            
            AVRational avbase = av_make_q(1, AV_TIME_BASE);
            int duration = av_rescale_q(pkt.duration, stream->time_base, avbase);
            videoDuration += duration;

            av_packet_rescale_ts(&pkt, stream->time_base, videobase);
            
            int64_t clockDuration = av_gettime_relative() - tmStart;
            delay = videoDuration - clockDuration;
            
            if (!hasPrefix(pkt.data, pkt.size))
            {
                *(int*)pkt.data = 0x01000000;
            }

            int64_t pts = pkt.pts;
            if (pts == AV_NOPTS_VALUE)
            {
                pts = av_rescale_q((tmStart + videoDuration), avbase, videobase);
            }

            MPacket mpkt;
            mpkt.duration = pkt.duration;
            mpkt.data = pkt.data;
            mpkt.size = pkt.size;
            mpkt.flags = pkt.flags;
            mpkt.pts = pts;

			AVRational scale = av_make_q(4, 4);

            mpkt.duration = pkt.duration * scale.den / scale.num;
            mpkt.pts = pkt.pts * scale.den / scale.num;

            caster_chl_write_video(handle, &mpkt);

            //comn::FileUtil::write(pkt.data, pkt.size, "m.hevc", true);

            if (printCount < 8)
            {
                printCount ++;
				
				std::cout << "video size: " << mpkt.size;
				std::cout << ", duration: " << mpkt.duration;
				std::cout << ", pts: " << mpkt.pts << std::endl;
            }
            
            av_usleep(1000*15);
        }
        else if (pkt.stream_index == audioIndex)
        {
            AVStream* stream = fmtContext->streams[pkt.stream_index];
            //int64_t pts = av_rescale_q(pkt.dts, stream->time_base, timebase);
            AVRational avbase = av_make_q(1, AV_TIME_BASE);
            int duration = av_rescale_q(pkt.duration, stream->time_base, avbase);
            audioDuration += duration;
            int64_t clockDuration = av_gettime_relative() - tmStart;
            delay = audioDuration - clockDuration;

            MPacket mpkt;
            mpkt.duration = pkt.duration;
            mpkt.data = pkt.data;
            mpkt.size = pkt.size;
            mpkt.flags = pkt.flags;
            mpkt.pts = pkt.pts;

            
            caster_chl_write_audio(handle, &mpkt);

            if (printCount < 8)
            {
                printCount ++;
				std::cout << "audio  duration: " << mpkt.duration;
				std::cout << ", pts: " << mpkt.pts;
            }
        }

		av_packet_unref(&pkt);

        //printf("delay: %f ms\n", delay / 1000.0);
        if (delay > 0)
        {
            //av_usleep(delay);
        }
        
        
    }

    std::cin.get();

    caster_chl_close(handle);
    handle = NULL;

    avformat_close_input(&fmtContext);

	return 0;
}




