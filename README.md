
# 实时媒体转发服务
接收设备端的音视频流, 通过rtsp/rtp转发给客户端.
可用于公网环境.

## 基本原理
信令用XMPP协议(部署的openfire), 媒体设备端和转发服务都注册到openfire. 客户端(VLC,ffplay等)通过RTSP请求播放时, 转发服务查找媒体设备, 要求媒体设备发送音视频流, 收到后转发给客户端.
媒体传输协议采用RTP协议, 底层支持UDP,TCP.

## 项目目录
* comn
    通用类库
* Demo*
    演示测试程序
* doc
    文档
* LiveDevice
    媒体设备推流类
* LiveRelayServer
    实时媒体流转发服务
* RtpCaster
    RTP发送,接收类库
* RtspCaster
    RTSP/RTP发送类库
* RtspRelay
    RTP->RTSP/RTP的媒体转发类库
* third_party
    第三方类库
* tools
    工具


## 编译
整个项目都采用cmake构建(包括第三方类库)

### windows
支持vs2008/vs2015. 构建命令:
进入LiveRelay目录
mkdir vs
cd vs
cmake ..
cmake --build .
如果要构建发布版本, 则运行
cmake --build --config release

### linux
cd LiveRelay
mkdir build && cd build
cmake .. && make

打包, 执行
cpack
输出 LiveRelayServer*.deb



### arm 交叉编译
cd LiveRelay
mkdir buildarm && cd buildarm
cmake .. -DCMAKE_TOOLCHAIN_FILE=../toolchain.cmake
make



## 部署

### ARM 设备端
arm 需要 LiveDevice, RtpCaster 这2个动态库. 
LiveDevice.ini 放在当前目录或者 /etc/ 目录下. 该配置文件必须设置本机地址和XMPP服务器地址和账户密码


### 服务器
* 部署OpenFire(XMPP服务器)

* 给设备和媒体转发服务配置账户

* 安装部署LiveRelayServer

安装 LiveRelayServer*.deb . 命令如下:

```
sudo dpkg --install LiveRelayServer*.deb
```

配置文件为LiveRelay.ini, 放在当前目录或者 /etc 下
该配置文件必须设置本机地址和XMPP服务器地址和账户密码

* 客户端

安装VLC播放器, 播放的地址格式为:  rtsp://{服务器地址}:1554/{设备账户名称}/{通道号}
比如: rtsp://192.168.3.113:1554/demo/0

* 检测系统运行状况
在openfire配置页面, 即  http://{服务器地址}:9090 
查看用户在线状况







          

    