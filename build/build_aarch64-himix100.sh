#/bin/sh

PWD=`pwd`
THIS_DIR=$(cd "$(dirname "$0")"; pwd)
ROOT_DIR=$THIS_DIR/..

# build app
mkdir $ROOT_DIR/aarch64-himix100
cd $ROOT_DIR/aarch64-himix100

cmake .. -DCMAKE_TOOLCHAIN_FILE=../aarch64-himix100.cmake
make -j20

# build pack
cpack

# zip
cd $THIS_DIR


