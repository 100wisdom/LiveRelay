#/bin/sh

PWD=`pwd`
THIS_DIR=$(cd "$(dirname "$0")"; pwd)
ROOT_DIR=$THIS_DIR/..

# build app
mkdir $ROOT_DIR/himix200
cd $ROOT_DIR/himix200

cmake .. -DCMAKE_TOOLCHAIN_FILE=../himix200.cmake
make -j20

# build pack
cpack

# zip
cd $THIS_DIR


