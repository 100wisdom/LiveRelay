
@rem created: 2016-04-19
@rem  author: chuanjiang.zh@qq.com
@rem   

set DIR_BUILD=%~dp0

@echo off

set DIR_PROJECT=%DIR_BUILD%..

mkdir %DIR_PROJECT%\vs
cd /D %DIR_PROJECT%\vs

cmake .. 
cmake --build . --config Release

@rem cpack 

cd /D %DIR_BUILD%

