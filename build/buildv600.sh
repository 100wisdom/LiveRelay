#/bin/sh

PWD=`pwd`
THIS_DIR=$(cd "$(dirname "$0")"; pwd)
ROOT_DIR=$THIS_DIR/..

# build app
mkdir $ROOT_DIR/hisiv600
cd $ROOT_DIR/hisiv600

cmake .. -DCMAKE_TOOLCHAIN_FILE=../hisiv600.cmake
make -j20

# build pack
cpack

# zip
cd $THIS_DIR


