//============================================================================
// Name        : Main.cpp
// Author      : chuanjiang.zh@qq.com
// Version     :
// Copyright   : 
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <iostream>

#include "AppProperties.h"
#include "RtspRelay.h"
#include "Socket.h"
#include "CLog.h"
#include "OptionParser.h"
#include "TStringUtil.h"

#include "LiveRelayServer.h"
#include "Version.h"

static bool s_finished = false;
static bool s_receivedHUP = false;
static comn::Event  g_event;


static void signalHandler(int signo)
{
#ifndef _WIN32
    if(signo == SIGHUP)
    {
        s_receivedHUP = true;
        return;
    }
#endif

    s_finished = true;
    g_event.post();
}



void setupLog()
{
    CLog::setLogger(CLog::COUT);
    CLog::setLogger(CLog::FILE);
    CLog::setFileParam("LiveRelay.log", 4, 4);
}

void setupSignal()
{

#ifdef _WIN32
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
#else
    struct sigaction act, oldact;
    memset(&act, 0, sizeof(struct sigaction));
    act.sa_handler = signalHandler;
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGALRM);
    sigaddset(&act.sa_mask, SIGINT);
    sigaddset(&act.sa_mask, SIGTERM);
    sigaddset(&act.sa_mask, SIGTSTP);
    sigaction(SIGALRM, &act, &oldact);
    sigaction(SIGINT, &act, &oldact);
    sigaction(SIGTERM, &act, &oldact);
    sigaction(SIGTSTP, &act, &oldact);
    act.sa_flags = 0;

#endif
}


void handleCommand(LiveRelayServer& svr, const std::string& line)
{

}


int main(int argc, char *argv[])
{
	OptionParser parser;
	parser.setUsage("%prog [option]");
	std::string strVersion = comn::StringUtil::format("%s Version %s\nCopyright (c) 2015, %s",
			APP_NAME, APP_VERSION, APP_VENDOR);
	parser.setVersion(strVersion);
	parser.enableHelp();
	parser.enableVersion();
	parser.addOption('d', "daemon", "run as daemon");

	if (!parser.parse(argc, argv))
	{
		parser.usage();
		return -1;
	}

	if (parser.hasHelpOption())
	{
		parser.usage();
		return 0;
	}

	if (parser.hasVersionOption())
	{
		parser.version();
		return 0;
	}

	bool isDaemon = parser.hasOption("daemon");

    comn::SocketContext socketContext;

#ifdef _WIN32
#else
    if (isDaemon)
    {
        daemon(0, 1);
    }
#endif //

    setupSignal();

    AppProperties::setPath("LiveRelay.ini");
    setupLog();

    LiveRelayServer relayServer;
    relayServer.start();

    std::string line;
	while (!s_finished)
	{
		if (isDaemon)
		{
			g_event.timedwait(1000);
		}
		else
		{
			std::cout << ">> ";
			std::istream& istrm = std::getline(std::cin, line);
			if (!istrm.good())
			{
				std::cin.clear();
				g_event.timedwait(1000);
				continue;
			}
		}

		if ((line == "q") || (line == "quit"))
		{
			break;
		}
		else
		{
			handleCommand(relayServer, line);
		}

		if(s_receivedHUP)
		{
			s_receivedHUP = false;
		}
	}

	relayServer.stop();

    return 0;
}
