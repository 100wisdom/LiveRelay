/*
 * Version.h
 *
 *  Created on: 2016年5月2日
 *      Author: terry
 */

#ifndef VERSION_H_
#define VERSION_H_


#define APP_NAME    "LiveRelayServer"
#define APP_VERSION "1.1.18"
#define APP_VENDOR  "www.100wits.com"
#define APP_DESC    "live relay server"



#endif /* VERSION_H_ */
