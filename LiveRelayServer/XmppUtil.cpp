/*    file: XmppUtil.cpp
 *    desc:
 * 
 * created: 2017-03-16
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include "XmppUtil.h"

const char* toString(IQ::IqType type)
{
	if (type == IQ::Get)
	{
		return "Get";
	}
	else if (type == IQ::Set)
	{
		return "Set";
	}
	else if (type == IQ::Result)
	{
		return "Result";
	}
	else if (type == IQ::Error)
	{
		return "Error";
	}
	return "Invalid";
}


