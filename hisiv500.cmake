# this is required
SET(CMAKE_SYSTEM_NAME Linux)

# specify the cross compiler
SET(CMAKE_C_COMPILER    arm-hisiv500-linux-gcc)
SET(CMAKE_CXX_COMPILER  arm-hisiv500-linux-g++)
SET(CMAKE_STRIP	arm-hisiv500-linux-strip)

# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH  /opt/hisi-linux/x86-arm/arm-hisiv500-linux )

# search for programs in the build host directories (not necessary)
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
