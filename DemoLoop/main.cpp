/*    file: main.cpp
 *    desc:
 * 
 * created: 2016-04-06
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include <stdio.h>
#include <iostream>

#include "BasicType.h"
#include "RtspCaster.h"
#include "Ffmpeg.h"
#include "SharedPtr.h"
#include "TFileUtil.h"
#include <assert.h>
#include "MediaDevice.h"

struct Application
{
public:
	Application()
    {
        caster_init(554);
    }

    ~Application()
    {
        caster_quit();
    }
};

void DemoMediaStreamCallback(MPacket* pkt, void* user, int channel)
{
	HANDLE handle = user;
	if (pkt->type == MTYPE_VIDEO)
	{
		caster_chl_write_video(handle, pkt);
	}
	else
	{
		caster_chl_write_audio(handle, pkt);
	}
}

int main(int argc, char** argv)
{
	av_register_all();
	
	if (argc <= 1)
    {
        return EINVAL;
    }

	std::string filename = argv[1];

    Application app;
	MediaDevice	device;

	if (!device.open(0, filename))
	{
		return 0;
	}

	MFormat fmt;
	memset(&fmt, 0, sizeof(fmt));
	device.getStreamFormat(&fmt);
	
    HANDLE handle = NULL;
    int rc = caster_chl_open(&handle, "demo", &fmt);

	device.setStreamCallback(DemoMediaStreamCallback, handle);
	device.startStream(NULL);

    std::cin.get();

	device.stopStream();

    caster_chl_close(handle);

	return 0;
}




