/*
 * MediaDevice.h
 *
 *  Created on: 2017年5月26日
 *      Author: chuanjiang.zh
 */

#ifndef MEDIADEVICE_H_
#define MEDIADEVICE_H_

#include "BasicType.h"
#include <string>
#include "FfmpegMediaReader.h"
#include "LoopReader.h"
#include "TThread.h"
#include "TEvent.h"
#include "TimePoint.h"
#include "RtspCaster.h"


 /**
 * 媒体流回调
 * @param pkt	媒体包
 * @param user	回调环境
 * @param channel	通道
 */
typedef void(*MediaStreamCallback)(MPacket* pkt, void* user, int channel);

class MediaDevice : public comn::Thread
{
public:
	MediaDevice();
	virtual ~MediaDevice();

	bool open(int chl, const std::string& url);
	void close();
	bool isOpen();

	int startStream(MFormat* fmt);

	int stopStream();

	int getStreamFormat(MFormat* fmt);

	int setStreamCallback(MediaStreamCallback cb, void* user);

protected:
	virtual int run();

	void firePacket(AVPacket& pkt);

protected:
	LoopReader	m_reader;
	MediaStreamCallback	m_streamCallback;
	void*	m_streamUser;

	av::TimePoint	m_timePoint;
	int		m_channel;
	std::string	m_url;

};

#endif /* MEDIADEVICE_H_ */
