CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

include_directories(${COMN_DIR}/include)
include_directories(${PROJECT_ROOT}/RtspCaster)
include_directories(${DIR_FFMPEG}/include)

if (MSVC)
include_directories(${THIRD_PARTY}/stdint)
endif()

link_directories(${PROJECT_ROOT}/lib)
link_directories(${COMN_DIR}/lib)
link_directories(${DIR_FFMPEG}/lib)
link_directories(${DIR_FFMPEG}/bin)

if (MSVC)
	SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /SAFESEH:NO")
else()
endif()

aux_source_directory(. SOURCES)
add_executable(DemoLoop ${SOURCES})

target_link_libraries(DemoLoop
	RtspCaster
	${LIB_FFMPEG}
	)

