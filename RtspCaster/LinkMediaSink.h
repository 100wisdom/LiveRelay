/*
 * LinkMediaSink.h
 *
 *  Created on: 2015年11月24日
 *      Author: terry
 */

#ifndef LINKMEDIASINK_H_
#define LINKMEDIASINK_H_

#include "MediaStream.h"

namespace av
{

class LinkMediaSink : public MediaSink
{
public:
	explicit LinkMediaSink(MediaSink* pSink):
		m_sink(pSink)
	{
	}

	virtual ~LinkMediaSink()
	{

	}

    void resetSink(MediaSink* pSink)
    {
        m_sink = pSink;
    }

    virtual void onMediaFormat(const MediaFormat& fmt)
    {
    	if (m_sink)
    	{
    		m_sink->onMediaFormat(fmt);
    	}
    }

    virtual void onMediaPacket(MediaPacketPtr& pkt)
    {
        fireMediaPacket(pkt);
    }

    virtual void onMediaEvent(int event)
    {
    	if (m_sink)
    	{
    		m_sink->onMediaEvent(event);
    	}
    }

protected:
    void fireMediaPacket(MediaPacketPtr& pkt)
    {
        if (m_sink)
        {
            m_sink->onMediaPacket(pkt);
        }
    }

protected:
	MediaSink*	m_sink;

};


typedef std::shared_ptr< LinkMediaSink >	LinkMediaSinkPtr;

}




#endif /* LINKMEDIASINK_H_ */
