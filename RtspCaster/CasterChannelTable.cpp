/*
 * CasterChannelTable.cpp
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#include "CasterChannelTable.h"
#include <algorithm>
#include "TSingleton.h"


namespace av
{

typedef comn::Singleton< CasterChannelTable >		CasterChannelTableSingleton;


CasterChannelTable& CasterChannelTable::instance()
{
	return CasterChannelTableSingleton::instance();
}


CasterChannelTable::CasterChannelTable()
{
}

CasterChannelTable::~CasterChannelTable()
{
}

struct EqualToName : public std::binary_function< void*, CasterChannelPtr, bool >
{
	EqualToName(const std::string& name):
		m_name(name)
	{
	}

	bool operator () (void* handle, CasterChannelPtr& chl) const
	{
        if (!chl)
        {
            return false;
        }
		return m_name == chl->getName();
	}

	std::string m_name;

};


CasterChannelPtr CasterChannelTable::findWithName(const std::string& name)
{
	EqualToName func(name);
	void* handle = NULL;
	CasterChannelPtr chl;
	findIf(func, handle, chl);
	return chl;
}

bool CasterChannelTable::exists(const std::string& name)
{
	CasterChannelPtr chl = findWithName(name);
	return chl.get() != NULL;
}


} /* namespace av */
