/*
 * CRtspServer.h
 *
 *  Created on: 2016年1月17日
 *      Author: terry
 */

#ifndef CRTSPSERVER_H_
#define CRTSPSERVER_H_

#include "RtspServer.h"
#include "TThread.h"
#include "SharedPtr.h"
#include "TCriticalSection.h"
#include "UserAuthDatabase.h"

class TaskScheduler;
class UsageEnvironment;
class RTSPServer;


namespace av
{

class RtspSessionListener
{
public:
    virtual ~RtspSessionListener() {}

    virtual void onRequestSession(const std::string& name) = 0;

    virtual void onCreateSession(const std::string& name) =0;

    virtual void onDestroySession(const std::string& name) =0;

    virtual void onSessionEvent(const std::string& name, int eventType) =0;

};



class Server;

class CRtspServer: public RtspServer , public comn::Thread, public RtspSessionListener
{
public:
	CRtspServer();
	virtual ~CRtspServer();

	virtual int start(const char* ip, int port);

	virtual void stop();

	virtual bool isStarted();

    virtual bool closeSession(const std::string& name);

    virtual void setCallback(RtspCasterEventCallback cb, void* context);

	virtual bool setAuthFile(const char* filepath);

	virtual bool closeClientSession(const std::string& name);


protected:
	virtual int run();
	virtual void doStop();

    virtual void onRequestSession(const std::string& name);

    virtual void onCreateSession(const std::string& name);

    virtual void onDestroySession(const std::string& name);

    virtual void onSessionEvent(const std::string& name, int eventType);

protected:
	typedef std::shared_ptr< TaskScheduler >	TaskSchedulerPtr;
	typedef std::shared_ptr< UsageEnvironment >	UsageEnvironmentPtr;

	TaskSchedulerPtr	m_scheduler;
	UsageEnvironmentPtr m_env;
	std::shared_ptr< Server >	m_server;
	volatile char	m_watchExit;

    RtspCasterEventCallback m_callback;
    void* m_context;
    
    comn::CriticalSection   m_cs;

	std::shared_ptr<UserAuthDatabase>	m_authDatabase;

};


} /* namespace av */

#endif /* CRTSPSERVER_H_ */
