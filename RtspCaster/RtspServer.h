/*
 * RtspServer.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef RTSPSERVER_H_
#define RTSPSERVER_H_

#include "BasicType.h"
#include <string>
#include "RtspCaster.h"

namespace av
{


class RtspServer
{
public:
	virtual ~RtspServer() {}

	virtual int start(const char* ip, int port) =0;

	virtual void stop() =0;

	virtual bool isStarted() =0;

    virtual bool closeSession(const std::string& name) =0;

    virtual void setCallback(RtspCasterEventCallback cb, void* context) =0;

	virtual bool setAuthFile(const char* filepath) =0;

	virtual bool closeClientSession(const std::string& name) =0;

};


}



#endif /* RTSPSERVER_H_ */
