/*
 * AudioFramedSource.h
 *
 *  Created on: 2015年11月26日
 *      Author: terry
 */

#ifndef AUDIOFRAMEDSOURCE_H_
#define AUDIOFRAMEDSOURCE_H_

#include "BasicType.h"
#include "FramedSource.hh"
#include "LinkMediaSink.h"
#include "MediaPacketQueue.h"


namespace av
{

class AudioFramedSource: public FramedSource, public av::MediaSink
{
public:
	AudioFramedSource(UsageEnvironment& env, MediaSourcePtr source, int clockRate);
	virtual ~AudioFramedSource();

	virtual void doGetNextFrame();
    virtual void doStopGettingFrames();

protected:
    virtual void onMediaFormat(const MediaFormat& fmt);

    virtual void onMediaPacket(MediaPacketPtr& pkt);

    virtual void onMediaEvent(int event);

    static void taskFunc(void* clientData);

    bool onTask();

protected:
    MediaSourcePtr 		m_source;
    LinkMediaSinkPtr    m_linkSink;
    MediaPacketQueue    m_pktQueue;
    EventTriggerId  	m_taskID;
    int     m_streamEvent;
    struct timeval  m_startTime;
    int		m_clockRate;

	int64_t	m_lastPts;
	bool	m_idle;

};


} /* namespace av */

#endif /* AUDIOFRAMEDSOURCE_H_ */
