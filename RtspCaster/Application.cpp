/*
 * Application.cpp
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#include "Application.h"


typedef comn::Singleton< Application >		ApplicationSingleton;


Application& Application::instance()
{
	return ApplicationSingleton::instance();
}

int Application::reclamation = 60;

Application::Application()
{
}

Application::~Application()
{
    
}


int Application::init()
{
	return 0;
}

void Application::quit()
{
	m_server.stop();

	m_chlTable.clear();
}

av::CRtspServer& Application::getRtspServer()
{
	return m_server;
}

av::CasterChannelTable& Application::getChannelTable()
{
	return m_chlTable;
}


