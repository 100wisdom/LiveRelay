/*    file: RtspConfig.cpp
 *    desc:
 * 
 * created: 2017-11-16
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include "RtspConfig.h"
#include "RTPInterface.hh"

int RtspConfig::reclamation = 60;

int RtspConfig::maxVideoQueueSize = 30;

int RtspConfig::maxAudioQueueSize = 30;

void RtspConfig::forceCompleteFrame(bool forced)
{
	RTPInterface::s_forceSendToSucceed = forced;
}

int RtspConfig::sendBufferSize = 1024 * 1024 * 1;

int RtspConfig::packetBufferSize = 1024 * 1024 * 1;