/*
 * AudioMediaSubsession.h
 *
 *  Created on: 2015年11月27日
 *      Author: terry
 */

#ifndef AUDIOMEDIASUBSESSION_H_
#define AUDIOMEDIASUBSESSION_H_

#include <OnDemandServerMediaSubsession.hh>
#include "MediaStream.h"


namespace av
{

class AudioMediaSubsession: public OnDemandServerMediaSubsession
{
public:
	AudioMediaSubsession(UsageEnvironment& env, portNumBits initPort,
			MediaSourcePtr mediaSource, const std::string& entry);
	virtual ~AudioMediaSubsession();

    virtual char const* sdpLines();

    virtual FramedSource* createNewStreamSource(unsigned clientSessionId,
				          unsigned& estBitrate);

      // "estBitrate" is the stream's estimated bitrate, in kbps
    virtual RTPSink* createNewRTPSink(Groupsock* rtpGroupsock,
			        unsigned char rtpPayloadTypeIfDynamic,
			        FramedSource* inputSource);

protected:
    MediaSourcePtr m_mediaSource;
	MediaFormat		m_mediaFormat;
    std::string  m_entry;


};

} /* namespace av */

#endif /* AUDIOMEDIASUBSESSION_H_ */
