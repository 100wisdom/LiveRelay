/*    file: RtspConfig.h
 *    desc:
 *   
 * created: 2017-11-16
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined RTSPCONFIG_H_
#define RTSPCONFIG_H_

class RtspConfig
{
public:
	static int reclamation;

	static int maxVideoQueueSize;

	static int maxAudioQueueSize;

	static void forceCompleteFrame(bool forced);

	static int sendBufferSize;

    static int packetBufferSize;

};


#endif //RTSPCONFIG_H_

