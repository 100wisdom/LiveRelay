/*
 * H264FramedSource.h
 *
 *  Created on: 2015年11月27日
 *      Author: terry
 */

#ifndef H264FRAMEDSOURCE_H_
#define H264FRAMEDSOURCE_H_

#include "BasicType.h"
#include <FramedSource.hh>
#include "LinkMediaSink.h"
#include "MediaPacketQueue.h"
#include "MediaStream.h"


namespace av
{

class H264FramedSource: public FramedSource, public av::MediaSink
{
public:
	H264FramedSource(UsageEnvironment& env, MediaSourcePtr& mediaSource, MediaFormat& fmt, int clockRate);
	virtual ~H264FramedSource();

	virtual void doGetNextFrame();
    virtual void doStopGettingFrames();

    MediaSourcePtr getMediaSource();

protected:
    virtual void onMediaFormat(const MediaFormat& fmt);

    virtual void onMediaPacket(MediaPacketPtr& pkt);

    virtual void onMediaEvent(int event);

    static void taskFunc(void* clientData);

    bool onTask();

    bool isPacketEmpty();

protected:
    MediaSourcePtr m_mediaSource;
    MediaFormat	m_fmt;
    LinkMediaSinkPtr    m_linkSink;

    MediaPacketQueue    m_pktQueue;
    EventTriggerId  	m_taskID;
    int     m_streamEvent;

    MediaPacketPtr	m_pkt;
    int	m_pktOffset;

    bool    m_needIFrame;	/// 是否必须发送的第一个帧就是关键帧
    struct timeval  m_startTime;
    int		m_clockRate;
	int64_t	m_lastPts;
	
	bool	m_idle;

};


} /* namespace av */

#endif /* H264FRAMEDSOURCE_H_ */
