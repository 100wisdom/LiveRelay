/*
 * RtpPort.h
 *
 *  Created on: 2015年7月6日
 *      Author: terry
 */

#ifndef RTPPORT_H_
#define RTPPORT_H_

#include "BasicType.h"

namespace av
{

/**
 * RTP端口管理类,控制端口使用范围
 */
class RtpPort
{
public:
	RtpPort();
	virtual ~RtpPort();

	/**
	 * 设置端口范围
	 * @param minPort	最小端口
	 * @param maxPort	最大端口
	 */
	static void setRange(uint16_t minPort, uint16_t maxPort);

	/**
	 * 生成端口
	 * @return
	 */
	static uint16_t make();

	/**
	 * 获取可用端口数量, 即端口范围长度的一半
	 * @return
	 */
	static size_t count();

};



} /* namespace av */

#endif /* RTPPORT_H_ */
