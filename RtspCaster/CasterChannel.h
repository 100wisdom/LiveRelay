/*
 * CasterChannel.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef CASTERCHANNEL_H_
#define CASTERCHANNEL_H_

#include "MediaStream.h"


namespace av
{

class CasterChannel : public MediaSource
{
public:
	virtual ~CasterChannel() {}

	virtual int open(const char* name, const MediaFormat& fmt) =0;

	virtual std::string getName() =0;

	virtual bool setFormat(const MediaFormat& fmt) =0;

	virtual int write(MediaPacketPtr& pkt) =0;

    virtual void setVideoProp(const uint8_t* prop, int size) =0;

	virtual MediaFormat& getFormat() = 0;
};


typedef std::shared_ptr< CasterChannel >		CasterChannelPtr;



} /* namespace av */

#endif /* CASTERCHANNEL_H_ */
