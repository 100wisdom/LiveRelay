/*
 * H265MediaSubsession.h
 *
 *  Created on: 2016年4月7日
 *      Author: terry
 */

#ifndef H265MEDIASUBSESSION_H_
#define H265MEDIASUBSESSION_H_

#include <OnDemandServerMediaSubsession.hh>
#include "MediaStream.h"

namespace av
{

class H265MediaSubsession: public OnDemandServerMediaSubsession
{
public:
	H265MediaSubsession(UsageEnvironment& env, portNumBits initPort,
        MediaSourcePtr mediaSource, const std::string& entry);
	virtual ~H265MediaSubsession();

    virtual char const* sdpLines();

    virtual FramedSource* createNewStreamSource(unsigned clientSessionId,
				          unsigned& estBitrate);

      // "estBitrate" is the stream's estimated bitrate, in kbps
    virtual RTPSink* createNewRTPSink(Groupsock* rtpGroupsock,
			        unsigned char rtpPayloadTypeIfDynamic,
			        FramedSource* inputSource);

    virtual void setStreamSourceScale(FramedSource* inputSource, float scale);

    virtual void closeStreamSource(FramedSource* inputSource);

    std::string getFmtp(int pt, const MediaFormat& fmt);

protected:
    std::string makeFmtp(int pt, const std::string& vps, const std::string& sps, const std::string& pps);

protected:
    MediaSourcePtr m_mediaSource;
    std::string m_entry;


};


} /* namespace av */

#endif /* H265MEDIASUBSESSION_H_ */
