/*
 * CCasterChannel.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef CCASTERCHANNEL_H_
#define CCASTERCHANNEL_H_

#include "CasterChannel.h"
#include "MediaSourceT.h"


namespace av
{

class CCasterChannel: public MediaSourceT< CasterChannel >
{
public:
	CCasterChannel();
	virtual ~CCasterChannel();

	virtual int open(const char* name, const MediaFormat& fmt);

	virtual int open(const std::string& url, const std::string& params);

	virtual void close();

	virtual bool isOpen();

	virtual std::string getName();

    virtual bool setFormat(const MediaFormat& fmt);

    virtual int write(MediaPacketPtr& pkt);

    virtual void setVideoProp(const uint8_t* prop, int size);

	virtual MediaFormat& getFormat();

protected:
    void parseParamSet(MediaPacket& pkt);
    bool parseH264ParamSet(MediaPacket& pkt);
    bool parseH265ParamSet(MediaPacket& pkt);

    void parseH264Nalu(const uint8_t* data, size_t length);
    void parseH265Nalu(const uint8_t* data, size_t length);

    void correctDuration(MediaPacket& pkt);

protected:
    std::string m_vps;
    std::string m_sps;
    std::string m_pps;
    
    int64_t m_videoLastPts;
    int64_t m_audioLastPts;

};


} /* namespace av */

#endif /* CCASTERCHANNEL_H_ */
