/*
 * Md5Digest.cpp
 *
 *  Created on: 2017年10月27日
 *      Author: terry
 */

#include "Md5Digest.h"
#include "Md5.h"
#include "TStringCast.h"

namespace util
{

Md5Digest::Md5Digest()
{
}

Md5Digest::~Md5Digest()
{
}

std::string Md5Digest::make(const std::string& data)
{
	MD5Context context;
	MD5Init(&context);
	MD5Update(&context, (const unsigned char *)data.c_str(), data.size());
	//MD5Pad(&context);

	unsigned char  result[16] = {0};
	MD5Final(result, &context);

	return comn::StringCast::toHexString(result, 16);
}


} /* namespace util */
