/*
 * UserAuthDatabase.h
 *
 *  Created on: 2017年10月27日
 *      Author: terry
 */

#ifndef USERAUTHDATABASE_H_
#define USERAUTHDATABASE_H_

#include "GenericMediaServer.hh"
#include "AuthUserFile.h"

class UserAuthDatabase : public UserAuthenticationDatabase
{
public:
	UserAuthDatabase(char const* realm = NULL, Boolean passwordsAreMD5 = true);
	virtual ~UserAuthDatabase();

	virtual void addUserRecord(char const* username, char const* password);
	virtual void removeUserRecord(char const* username);

	virtual char const* lookupPassword(char const* username);

	bool load(const char* filepath);

protected:
	util::AuthUserFile m_userFile;

};

#endif /* USERAUTHDATABASE_H_ */
