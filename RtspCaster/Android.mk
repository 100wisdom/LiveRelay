LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := RtspCaster

LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../comn/include \
	$(LOCAL_PATH)/../third_party/live/BasicUsageEnvironment/include \
	$(LOCAL_PATH)/../third_party/live/groupsock/include \
	$(LOCAL_PATH)/../third_party/live/liveMedia/include \
	$(LOCAL_PATH)/../third_party/live/UsageEnvironment/include \
	
	
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)

LOCAL_CPPFLAGS += -fexceptions 

SRC_FILES := $(wildcard $(LOCAL_PATH)/*.cpp)
SRC_FILES := $(SRC_FILES:$(LOCAL_PATH)/%=%)

LOCAL_SRC_FILES += $(SRC_FILES)

LOCAL_LDLIBS += -L$(SYSROOT)/usr/lib -llog -landroid

LOCAL_STATIC_LIBRARIES := \
	liveMedia comn
	
LOCAL_SHARED_LIBRARIES := \
	
	

include $(BUILD_SHARED_LIBRARY)

