/*
 * MediaType.h
 *
 *  Created on: 2016年1月12日
 *      Author: terry
 */

#ifndef MEDIATYPE_H_
#define MEDIATYPE_H_

#include "BasicType.h"
#include <string>

#ifndef MKBETAG
#define MKBETAG(a,b,c,d) ((d) | ((c) << 8) | ((b) << 16) | ((unsigned)(a) << 24))
#endif //MKBETAG

namespace av
{


enum MediaType
{
    MEDIA_TYPE_NONE  = -1,
    MEDIA_TYPE_VIDEO = 0,
    MEDIA_TYPE_AUDIO,
	MEDIA_TYPE_DATA,
	AVMEDIA_TYPE_SUBTITLE
};


enum StreamState
{
    STATE_STOPPED = 0,
	STATE_PAUSED,
	STATE_PLAYING,
    STATE_CLOSED
};


enum StreamEvent
{
	STREAM_EVENT_END = 0x10

};

enum MediaCodec
{
    MEDIA_CODEC_NONE = 0,

    MEDIA_CODEC_MPEG2 = 2,

    MEDIA_CODEC_H264 = 28,
    MEDIA_CODEC_VP8  = 141,
    MEDIA_CODEC_MPEG4= 13,
    MEDIA_CODEC_THEORA=31,
    MEDIA_CODEC_HEVC = 174,

    MEDIA_CODEC_G711U = 65542,
    MEDIA_CODEC_G711A,

    MEDIA_CODEC_MP3 = 0x15001,
    MEDIA_CODEC_AAC = 0x15002,
    MEDIA_CODEC_AC3 = 0x15003,
    MEDIA_CODEC_VORBIS = 0x15005,
    MEDIA_CODEC_OPUS = 0x1503d,
	MEDIA_CODEC_RAW = 0x10101010

};

enum MediaFlag
{
    MEDIA_FLAG_KEY = 0x01
};









} /* namespace av */

#endif /* MEDIATYPE_H_ */
