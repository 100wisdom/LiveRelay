/*
 * Md5Digest.h
 *
 *  Created on: 2017年10月27日
 *      Author: terry
 */

#ifndef MD5DIGEST_H_
#define MD5DIGEST_H_

#include "BasicType.h"
#include <string>

namespace util
{

class Md5Digest
{
public:
	Md5Digest();
	virtual ~Md5Digest();

	static std::string make(const std::string& data);

};

} /* namespace util */

#endif /* MD5DIGEST_H_ */
