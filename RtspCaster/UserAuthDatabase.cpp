/*
 * UserAuthDatabase.cpp
 *
 *  Created on: 2017年10月27日
 *      Author: terry
 */

#include "UserAuthDatabase.h"

UserAuthDatabase::UserAuthDatabase(char const* realm, Boolean passwordsAreMD5):
	UserAuthenticationDatabase(realm, passwordsAreMD5)
{
}

UserAuthDatabase::~UserAuthDatabase()
{
}

void UserAuthDatabase::addUserRecord(char const* username, char const* password)
{
	m_userFile.add(username, password, realm());
}

void UserAuthDatabase::removeUserRecord(char const* username)
{
	m_userFile.remove(username);
}

char const* UserAuthDatabase::lookupPassword(char const* username)
{
	size_t idx = m_userFile.find(username);
	if (idx == -1)
	{
		return NULL;
	}
	util::AuthUserFile::AuthUser& authUser = m_userFile.get(idx);
	return authUser.password.c_str();
}

bool UserAuthDatabase::load(const char* filepath)
{
	m_userFile.clear();
	return m_userFile.load(filepath);
}