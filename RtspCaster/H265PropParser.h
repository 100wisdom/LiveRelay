/*
 * H265PropParser.h
 *
 *  Created on: 2016年4月7日
 *      Author: terry
 */

#ifndef H265PROPPARSER_H_
#define H265PROPPARSER_H_

#include "BasicType.h"
#include <string>


namespace av
{

class H265PropParser
{
public:
	H265PropParser();
	virtual ~H265PropParser();

	static const size_t START_CODE_LENGTH = 4;
	static const uint8_t s_startCode[START_CODE_LENGTH];

	static const uint8_t* getStartCode();

	static bool startWithH264Code(const uint8_t* data, size_t length);
	static size_t findH264StartCode(const uint8_t* data, size_t length, size_t start);

	static bool splitPropSet(const uint8_t* data, size_t length,
					std::string& vps, std::string& sps, std::string& pps);


};


} /* namespace av */

#endif /* H265PROPPARSER_H_ */
