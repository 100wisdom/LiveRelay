/*
 * AuthUserFile.h
 *
 *  Created on: 2017年10月27日
 *      Author: terry
 */

#ifndef AUTHUSERFILE_H_
#define AUTHUSERFILE_H_

#include "BasicType.h"
#include <string>
#include <vector>

namespace util
{

class AuthUserFile
{
public:
	static const char* REALM;

public:
	AuthUserFile();
	virtual ~AuthUserFile();

	bool load(const char* filepath);

	bool check(const std::string& username, const std::string& password);

	bool check(const std::string& username, const std::string& password, const std::string& realm);

	size_t size() const;

	struct AuthUser
	{
		std::string username;
		std::string realm;
		std::string password;
	};

	AuthUser get(size_t idx) const;

	AuthUser& get(size_t idx);

	bool find(const std::string& username, AuthUser& authUser);

	size_t find(const std::string& username);

	bool exist(const std::string& username);

	void clear();
	
	bool remove(const std::string& username);

	bool add(const AuthUser& authUser);

	bool add(const std::string& username, const std::string& password, const std::string& realm);

	/// 获取第一个非空的Realm
	std::string getRealm();

protected:

protected:
	std::vector< AuthUser >	m_users;

};


} /* namespace util */

#endif /* AUTHUSERFILE_H_ */
