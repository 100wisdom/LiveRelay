/*
 * Application.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef APPLICATION_H_
#define APPLICATION_H_

#include "TSingleton.h"
#include "SharedPtr.h"
#include "CasterChannelTable.h"
#include "CRtspServer.h"


class Application
{
public:
	static Application& instance();

	static int reclamation;

public:
	Application();
	virtual ~Application();

	int init();

	void quit();

	av::CRtspServer& getRtspServer();

	av::CasterChannelTable& getChannelTable();

private:
	av::CRtspServer	m_server;
	av::CasterChannelTable	m_chlTable;


};

#endif /* APPLICATION_H_ */
