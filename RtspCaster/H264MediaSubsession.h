/*
 * H264MediaSubsession.h
 *
 *  Created on: 2015年11月27日
 *      Author: terry
 */

#ifndef H264MEDIASUBSESSION_H_
#define H264MEDIASUBSESSION_H_

#include <OnDemandServerMediaSubsession.hh>
#include "MediaStream.h"


namespace av
{

class H264MediaSubsession: public OnDemandServerMediaSubsession
{
public:
	H264MediaSubsession(UsageEnvironment& env, portNumBits initPort,
        MediaSourcePtr mediaSource, const std::string& entry);
	virtual ~H264MediaSubsession();

    virtual char const* sdpLines();

    virtual FramedSource* createNewStreamSource(unsigned clientSessionId,
				          unsigned& estBitrate);

      // "estBitrate" is the stream's estimated bitrate, in kbps
    virtual RTPSink* createNewRTPSink(Groupsock* rtpGroupsock,
			        unsigned char rtpPayloadTypeIfDynamic,
			        FramedSource* inputSource);

    virtual void setStreamSourceScale(FramedSource* inputSource, float scale);

    virtual void closeStreamSource(FramedSource* inputSource);

    std::string getFmtp(int pt, const MediaFormat& fmt);

protected:
    MediaSourcePtr m_mediaSource;
    std::string m_entry;

};



} /* namespace av */

#endif /* H264MEDIASUBSESSION_H_ */
