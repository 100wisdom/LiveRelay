/*    file: MediaPacket.cpp
 *    desc:
 * 
 * created: 2014-07-08 18:58:53
 *  author: zhengchuanjiang
 * company: 
 */

#include "MediaPacket.h"
#include "MediaType.h"


namespace av
{


MediaPacket::MediaPacket():
	data(),
	size(),
	type(),
	pts(),
	duration(),
	flags()
{
	data = m_buffer.data();
	size = m_buffer.size();
}

MediaPacket::~MediaPacket()
{
}

MediaPacket::MediaPacket(size_t length):
    data(),
    size(),
    type(),
    pts(),
    duration(),
    flags()
{
    data = m_buffer.data();
    size = m_buffer.size();

    ensure(length);
}

size_t	MediaPacket::capacity() const
{
    return m_buffer.capacity();
}

void MediaPacket::ensure(size_t length)
{
    m_buffer.ensure(length);

	data = m_buffer.data();
	size = m_buffer.size();
}

void MediaPacket::clear()
{
    m_buffer.clear();

    type = 0;
    pts = 0;
    duration = 0;
    flags = 0;

	data = m_buffer.data();
	size = m_buffer.size();
}

void MediaPacket::resize(size_t length)
{
    m_buffer.resize(length);

	data = m_buffer.data();
	size = m_buffer.size();
}

void MediaPacket::write(const uint8_t* p, size_t length)
{
    m_buffer.write(p, length);

	data = m_buffer.data();
	size = m_buffer.size();
}

void MediaPacket::set(const uint8_t* p, size_t length)
{
    m_buffer.clear();
    m_buffer.write(p, length);

	data = m_buffer.data();
	size = m_buffer.size();
}

bool MediaPacket::empty() const
{
    return (data == NULL) || (size == 0);
}

bool MediaPacket::hasFlags(int flags) const
{
    return ((this->flags & flags) != 0);
}

void MediaPacket::clone(const MediaPacket& pkt)
{
    copyProp(pkt);

    set(pkt.data, pkt.size);
    
}

void MediaPacket::copyProp(const MediaPacket& pkt)
{
    type = pkt.type;
    duration = pkt.duration;
    pts = pkt.pts;
    flags = pkt.flags;
}

bool MediaPacket::isVideo() const
{
    return type == MEDIA_TYPE_VIDEO;
}

bool MediaPacket::isKey() const
{
    return (flags & MEDIA_FLAG_KEY) != 0;
}


}
