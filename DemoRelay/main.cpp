/*    file: main.cpp
 *    desc:
 * 
 * created: 2016-05-01
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include <stdio.h>
#include <iostream>
#include <assert.h>

#include <string>

#include "RtspRelay.h"
#include "TStringUtil.h"
//#include "RtpMediaFormat.h"


class AppGuard
{
public:
    AppGuard()
    {
        relay_init(1554);
    }

    ~AppGuard()
    {
        relay_quit();
    }
};


int main(int argc, char** argv)
{
    AppGuard guard;

    HANDLE handle = 0;
    MFormat fmt;
    memset(&fmt, 0, sizeof(fmt));
    fmt.codec = MCODEC_H264;
    int rc = relay_chl_open(&handle, kProtocolUdp, "demo", &fmt);
    assert(rc == 0);

    char videoUrl[2048];
    char audioUrl[2048];
    rc = relay_chl_get_transport(handle, videoUrl, 2048, audioUrl, 2048);
    printf("local url: %s\n", videoUrl);

    std::string videoSource = comn::StringUtil::format("rtp://0.0.0.0:2000/video?pt=96&codec=H264&clock=90000");
    std::string audioSource;
    rc = relay_chl_set_source(handle, videoSource.c_str(), audioSource.c_str());

    while (true)
    {
        std::cout << ">>";
        std::string line;
        std::getline(std::cin, line);
        if (std::cin.good())
        {
            if ((line == "q") || (line == "quit"))
            {
                break;
            }
        }
        else
        {
            std::cin.clear();
        }
    }

    relay_chl_close(handle);


	return 0;
}




