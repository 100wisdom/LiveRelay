# this is required
SET(CMAKE_SYSTEM_NAME Linux)

# specify the cross compiler
SET(CMAKE_C_COMPILER    arm-hisiv400-linux-gcc)
SET(CMAKE_CXX_COMPILER  arm-hisiv400-linux-g++)
SET(CMAKE_STRIP	arm-hisiv400-linux-strip)

# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH  /opt/hisi-linux/x86-arm/arm-hisiv400-linux )

# search for programs in the build host directories (not necessary)
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
