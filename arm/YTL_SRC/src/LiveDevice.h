/*
 * LiveDevice.h
 *
 *  Created on: 2016年4月28日
 *      Author: terry
 */

#ifndef LIVEDEVICE_H_
#define LIVEDEVICE_H_

////////////////////////////////////////////////////////////////////////////

#ifdef WIN32

    #ifndef NOMINMAX
    #define NOMINMAX
    #endif //NOMINMAX

	#include <Windows.h>
#else

#endif //WIN32


////////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
    typedef signed char     int8_t;
    typedef unsigned char   uint8_t;
    typedef short           int16_t;
    typedef unsigned short  uint16_t;
    typedef int             int32_t;
    typedef unsigned        uint32_t;
    typedef long long       int64_t;
    typedef unsigned long long   uint64_t;
#else
    #include <stdint.h>
    typedef void*   HANDLE;
#endif //_MSC_VER


///////////////////////////////////////////////////////////////////
#ifdef WIN32
    #ifndef DLLEXPORT
    #define DLLEXPORT __declspec(dllexport)
    #endif //DLLEXPORT
#else
    #define DLLEXPORT
#endif //WIN32

///////////////////////////////////////////////////////////////////

namespace av
{

class DLLEXPORT LiveDevice
{
public:
	LiveDevice();
	virtual ~LiveDevice();

	static void startup();

	static void cleanup();

	static LiveDevice& instance();

	virtual void setVideoFormat(int width, int height, int fps, int codec) =0;

	virtual void setAudioFormat(int channels, int sampleRate, int codec) =0;

	virtual int open() =0;

	virtual void close() =0;

	virtual bool isOpen() =0;

	virtual void inputVideo(uint8_t* data, int size, int64_t pts) =0;

	virtual void inputAudio(uint8_t* data, int size, int64_t pts) =0;


};



} /* namespace av */

#endif /* LIVEDEVICE_H_ */
