#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H
#include <sys/time.h>
#define SUCCEED                             0
#define FAILED                              -1

#define COMMON_FILE_OPEN_ERR                1
#define COMMON_FILE_RDWR_ERR                2
#define COMMON_FILE_TOO_LARGE               3
#define COMMON_FILE_FORMAT_ERR              4
#define COMMON_VAR_NOT_MATCH                5

#define COMMON_TEST_MAIL                    1
#define COMMON_TEST_FTP                     2
#define COMMON_UPDATE_CONF                  3

#define MAX_NAME_SIZE                       32
#define MAX_FILE_SIZE                       40960
#ifdef BUILD_FOR_ARM_3518E
#define MAX_VIDEO_FRAM_SIZE                 (1024*400)
#elif defined BUILD_FOR_ARM
#define MAX_VIDEO_FRAM_SIZE                 (1024*256)
#else
#define MAX_VIDEO_FRAM_SIZE                 (1024*180)
#endif

#define PID_NAME_FILE                        "/var/mubox.pid"
#define EVENT_DETECT_PID                    "/tmp/event_detect.pid"
#define DEVICE_DISCOVER_PID                 "/tmp/device_discover.pid"
#define TEST_FTP_MAIL_FLAG                  "/tmp/test_ftp_mail_flag"

#define MEDIA_CTRL_FIFO                     "/tmp/my_fifo"
#define EVENT_FIFO                          "/tmp/event_fifo"
#define RECORD_PTHREAD_FIFO                 "/tmp/recod_fifo"
#define NOTIFY_EVENT_PROCESS                "/tmp/notify_event_process"
#define PTZ_FIFO                            "/tmp/ptz_fifo"
#define ORANGE_FIFO                         "/tmp/http_pic_fifo"
#define ORANGE_PIC_PATH                     "/tmp/jk.jpeg"
#define	WAYDE_TECH_RECV_FIFO_NAME	        "/tmp/cmd_r_fifo"
#define GET_THIRPART_ALARM_TCONFIG          "/mnt/config/tmpfs/get_tlxsy.cgi"
//#define THIRD_PART_ALARM_NOTIFY             "/tmp/nkzkcpi" //+3
#define THIRD_PART_ALARM_NOTIFY             "/tmp/fifo2com" //+3
#define P2P_STATUS_FILE                     "/tmp/p2p_status.ini"
#define KEY_EVENT_FIFO                      "/tmp/key_fifo"

#define FIFO_NAME                           "/tmp/my_fifo"
#define PLAY_AUDIO_FIFO                     "/tmp/audio_fifo"
#define SEND_AUDIO_FIFO                     "/tmp/send_audio_fifo"
#define SEND_P2P_VIDEO                      "/tmp/p2p2_video"
#define SEND_P2P_AUDIO                      "/tmp/p2p2_audio"
#define SEND_VIDEOSTREAM2                   "/tmp/send_video_fifo2"

#ifdef BUILD_FOR_MIPSEL
  #ifdef BUILD_FOR_ARM
	#define WEB_CONFIG_DIR        "/mnt/config/"
	#define THTTPD_CONFIG_FILE    "/mnt/config/thttpd/thttpd.conf"
	#define TTY_DEV               "/dev/ttySAK0"
 #elif defined BUILD_FOR_ARM_3518E
	#define WEB_CONFIG_DIR        "/mnt/config/"
	#define THTTPD_CONFIG_FILE    "/mnt/config/thttpd/thttpd.conf"
	#define TTY_DEV               "/dev/ttyS000"
 #else
	#define WEB_CONFIG_DIR        "/mnt/config/"
	#define THTTPD_CONFIG_FILE    "/mnt/config/thttpd/thttpd.conf"
	#define TTY_DEV               "/dev/ttyS0"
 #endif
#else
	#define WEB_CONFIG_DIR        "/home/hejh/code/ipcamera_project/web_server/webserver/web/"
	#define THTTPD_CONFIG_FILE    "/etc/boa/thttpd_conf"
	#define TTY_DEV               "/dev/ttyUSB0"
#endif

#define SETTING_FILE_DIR		"/usr/ipcam/bak/"
#define RUN_EXE_DIR			    "/usr/sbin/"
#define RECORTD_VIDEO_PATH      WEB_CONFIG_DIR"record/video"
#define SAVE_RECORD_INFO                WEB_CONFIG_DIR"record/"
#define SDCARD_DIR                      WEB_CONFIG_DIR"record/"

#define MEDIA_RECORD_FILE               WEB_CONFIG_DIR"get_record.cgi"
#define CAM_CTRL_SETING_FILE            WEB_CONFIG_DIR"get_camera_params.cgi"
#define ALARM_RECORD_FILE               WEB_CONFIG_DIR"get_params.cgi"
#define FTP_RECORD_FILE                 WEB_CONFIG_DIR"get_params.cgi"
#define SET_USERS_FILE                  WEB_CONFIG_DIR"get_params.cgi"
#define ALIAS_SETING_FILE               WEB_CONFIG_DIR"get_status.cgi"
#define TEMP_COFIG_FILE                 "temp_config.cgi"
#define P2P_CLIENT_SYS_CONF             WEB_CONFIG_DIR"npc_nts_client_config.ini"
#define WIFI_CONFIG_FILE                WEB_CONFIG_DIR"get_params.cgi"
#define RECORD_INFO_FILE                SAVE_RECORD_INFO"record_info.cgi"
#define RECORD_INFO_TEMP_FILE           SAVE_RECORD_INFO"record_info_tempfile.cgi"
#define EVENT_ALARM_FILE                WEB_CONFIG_DIR"alarm_record.cgi"
#define GET_RECORD_FILE                 SAVE_RECORD_INFO"get_record_file.cgi"
#define UUID_FILE                       "/tmp/ipcam.txt"
#define UPGRADE_SH		                "/usr/sbin/upgrade.sh"
//#define DYNAMIC_FILE                    WEB_CONFIG_DIR"get_dynamic.cgi"
#define DYNAMIC_FILE                    "/tmp/get_dynamic.cgi"
#define MAX_QUALITY_FILE               WEB_CONFIG_DIR"max_video_quality.cgi"
#define PHOTO_FILE_PATH                WEB_CONFIG_DIR"tmpfs/"
#define RESTORE_FACTORY_SETTING        "/usr/sbin/restore_factory.sh "
#define SYSTEM_UPGRADE_FLAG            "/tmp/upgrade_sys_flag"
#define UI_UPGRADE_FLAG                "/tmp/upgrade_ui_flag"
#define FEED_WATCHDOG                  "/usr/sbin/feed_watchdog.sh &"
#define PHOTO_LOCK_FLAG                   "/tmp/photo_lock"
#define MISC_RECORD_FILE               WEB_CONFIG_DIR"get_misc.cgi"
#define SDCARD_PATH                    "/mnt/config/record"
#define MOUNT_SDP1                      "mount  -o  fmask=111,sync /dev/mmcblk0p1 "SDCARD_PATH
#define MOUNT_SD                        "mount -rw /dev/mmcblk0  "SDCARD_PATH
#define UMOUNT_SD                       "umount  "SDCARD_PATH
#define UPDATE_SDCARD_FILES             "/usr/sbin/inser_sdcard_update_file.sh &"
#define SD_DEV_NAME                     "/dev/mmcblk0p1"
#define RECORD_FILE_NAME_PATH            WEB_CONFIG_DIR"record/"
#define RECORD_FILE_DIR                  RECORD_FILE_NAME_PATH"video/"
#define SAVE_RECORD_INFOFIE_DIR          WEB_CONFIG_DIR"record/"
#define DOWNLOAD_SERVER_PATH             "/IMG_Server/images/"
#define UPGRADE_ONLINE_CMD               "/usr/sbin/upgrade_online.sh "
#define REBOOT_MANULLY_FILE              "/mnt/config/reboot_manully_flag"
#define BUFFER_SIZE PIPE_BUF

#define RECORD_FILE_NAME_LENTH             256
enum mediaserver_command_type
{
     TYPE_SET_VIDEO_RESOLUTIONS=0,
     TYPE_SET_VIDEO_BRIGHTNEESS = 1,
     TYPE_SET_VIDEO_CONTRAST = 2,
     TYPE_SET_VIDEO_MODE = 3,
     TYPE_SET_VIDEO_FLIP = 5,
     TYPE_SET_VIDEO_FRAMERATE = 6,
     TYPE_SET_RTSP_PORT = 9,
     TYPE_SET_RESTORE_DEFALULT_CAMERA_SETTING =10,
     TYPE_SET_VIDEO_OSD_TIME = 11,
     TYPE_SET_VIDEO_OSD_STRING = 12,
     TYPE_REBOOT_DEVICE = 14,
     TYPE_MUTIDEVICE_CHANGED = 15,
     TYPE_DEVICE_INFO_CHANGED = 16,
     TYPE_OSD_ENABLE_CHANGED = 17,
     TYPE_ENABLE_MOTION = 18,
     TYPE_SET_KEY_FRAME = 19,
     TYPE_AUDIO_PLAY = 20,
     TYPE_AUDIO_RECORD = 21,
     TYPE_START_VIDEO_RECORD = 53,
     TYPE_STOP_VIDEO_RECORD = 52,
     TYPE_SET_MOTION_SENSITY = 99,
     TYPE_UPDATE_CONFIGS = 100,
     TYPE_MAIN_PTHREAD_WATCHDOG = 101,
     TYPE_SOFTWARE_RESTART =  102,
     TYPE_FORMAT_SDCARD = 103,
     TYPE_NOTIFY_IE = 104,
     TYPE_SETTING_FRAMRATE2=105,
     TYPE_UPGRADE_COMMAND= 800,
     TYPE_ALARM_WARING=801,
     TYPE_ALARM_SOUND = 106,
     TYPE_NNKNOW
};

typedef enum command_type
{
    COMMAND_TYPE_SET_VIDEO_RESOLUTIONS=0,
    COMMAND_TYPE_SET_VIDEO_BRIGHTNEESS = 1,
    COMMAND_TYPE_SET_VIDEO_CONTRAST = 2,
    COMMAND_TYPE_SET_VIDEO_MODE = 3,
    COMMAND_TYPE_SET_VIDEO_SATURATION=4,
    COMMAND_TYPE_SET_VIDEO_FLIP = 5,
    COMMAND_TYPE_SET_VIDEO_FRAMERATE = 6,
    COMMAND_TYPE_SET_VIDEO_HUE = 7,
    COMMAND_TYPE_SET_VIDEO_DEFAULTVALUE=8,
    COMMAND_TYPE_SET_RTSP_PORT = 9,
    COMMAND_TYPE_SET_RESTORE_DEFALULT_CAMERA_SETTING =10,
    COMMAND_TYPE_SET_VIDEO_OSD_TIME = 11,
    COMMAND_TYPE_SET_VIDEO_OSD_STRING = 12,
    COMMAND_TYPE_REBOOT_DEVICE = 14,
    COMMAND_TYPE_MUTIDEVICE_CHANGED = 15,
    COMMNAD_TYPE_DEVICE_INFO_CHANGED = 16,
    COMMAND_TYPE_OSD_ENABLED = 17,
    COMMAND_TYPE_ENABLE_MOTION = 18,
    COMMAND_TYPE_SET_KEY_FRAME = 19,
    COMMAND_TYPE_AUDIO_PLAY = 20,
    COMMAND_TYPE_AUDIO_RECORD = 21,
    COMMAND_TYPE_AUDIO_PLAY_VOLUME = 50,
    COMMAND_TYPE_AUDIO_RECORD_VOLUME = 51,
    COMMAND_TYPE_STOP_VIDEO_RECORD = 52,
    COMMAND_TYPE_START_VIDEO_RECORD = 53,
    COMMAND_TYPE_MOTION_SENSITY = 99,
    COMMAND_TYPE_UPDATE_CONFIGS = 100,
    COMMAND_TYPE_MAIN_PTHREAD_WATCHDOG = 101,
    COMMAND_TYPE_SOFTWARE_RESTART =  102,
    COMMAND_TYPE_FORMAT_SDCARD = 103,
    COMMAND_TYPE_NOTIFY_IE = 104,
    COMMAND_TYPE_CHANGED_FRAMERATE2=105,
    COMMAND_TYPE_UPDATE_FIRMWARE = 800,
    COMMAND_TYPE_ALARM_SOUND = 106,
    COMMAND_TYPE_ALARM_WARING = 801,
    COMMAND_TYPE_ALARM_DISABLE = 802,
    COMMNAD_TYPE_WAYDE_NOTIFY = 0x5a5a,
    COMMAND_TYPE_VIDEO_ZOOM = 803,
    COMMAND_TYPE_CAMERA_WORKMODE=804,
    COMMAND_TYPE_MAP_VIDEO_CACHE_BUFFER=805,
    COMMAND_TYPE_THIRD_PART_ALARM_STATUS =806,
    COMMAND_TYPE_UPDATE_ENLARG_VOLUME,
    COMMAND_TYPE_GET_LIGHT_NIGHT_MODE,
    COMMAND_TYPE_NETWORK_CHANGED,
    COMMAND_TYPE_UPDATE_FIRMWARE_PREPARE,
    COMMAND_TYPE_ENABLE_SPEAK,
    COMMAND_TYPE_UPGRADE_ONLINE_AUTO,
    COMMAND_TYPE_CHANGED_BITRATE,
    COMMAND_TYPE_NOTIFY_FROM_IDEALTECH_APP=111,
    COMMAND_TYPE_NNKNOW
}enum_command_type;

enum notify_event_processs_command_type
{
    TYPE_EVENT_PROCESS_START_RECORD = 0,
    TYPE_EVENT_PROCESS_STOP_RECORD = 1,
    TYPE_EVENT_PROCESS_IO_CLOSE = 2,
    TYPE_EVENT_PROCESS_IO_OPEN = 3,
    TYPE_EVENT_PROCESS_MOTION_HAPPEND = 4,
    TYPE_EVENT_PROCESS_GPIO_ALARM_HAPPEND = 6,
    TYPE_EVENT_PROCESS_FTP_TEST = 7,
    TYPE_EVENT_PROCESS_EMAIL_TEST =  8,
    TYPE_EVENT_PROCESS_PROCESS_QIUT = 9,
    TYPE_EVENT_PROCESS_UPDATE_CONFIG = 10,
    TYPE_EVENT_PROCESS_START_FORMAT_SDCARD = 11,
    TYPE_EVENT_PROCESS_AIDE_DOORS = 12,
    TYPE_EVENT_PROCESS_AIDE_MIC = 13,
    TYPE_EVENT_PROCESS_MOTION_ALARM_DISABLE = 14,
    TYPE_EVENT_PROCESS_RESTART,
    TYPE_EVENT_PROCESS_NOTIFY_IRCUT_STATE,
    TYPE_EVENT_PROCESS_BIRGHTNESS_NIGHT_MODE,
    TYPE_EVENT_PROCESS_BIRGHTNESS_NIGHT_MODE_INIT,
    TYPE_EVENT_PROCESS_ENABLE_SPEAKER,
    TYPE_EVENT_PROCESS_NUKNOW
};

typedef enum IRCUT_STAT_MODE
{//1表示大于阀值，0表示小于阀值需要切换IRCUT
    IRCUT_STATE_UNKNOW = 5,
    IRCUT_STATE_B1_R1 = 0x11,
    IRCUT_STATE_B1_R0 = 0x10,
    IRCUT_STATE_B0_R0 = 0x00,
    IRCUT_STATE_B0_R1 = 0x01
}enum_ircut_state;

typedef enum record_type
{
    RECORD_TYPE_GPIO = 10,
    RECORD_TYPE_MOTION = 01,
    RECORD_TYPE_OTHER = 11,
    RECORD_TYPE_TIMER = 00
     //gpio报警10，移动侦测01，其他录像11, 定时录像00
}enum_record_type;

enum instore_varible_index
{
    INSTORE_TYPE_MOTION_SENSITY= 0,
    INSTORE_TYPE_MOTION_ENABLE,
    INSTORE_TYPE_TIMEZONE_VALUE,
    INSTORE_TYOE_GPIO_ALARMED
};

enum instore_media_varible_index
{
    INSTORE_TYPE_RECORD_MOTION_ENBLE = 0,
    INSTORE_TYPE_RECORD_TIME_ENABLE
};
#if defined BUILD_FOR_ARM || defined BUILD_FOR_ARM_3518E
struct media_info
{
    int media_size;
    int bitrate;
    int media_ratemode;
    int key_frame;
    int quant;
    int min_qp1;
    int max_qp1;
    int media_size2;
    int bitrate2;
    int media_ratemode2;
    int key_frame2;
    int quant2;
    int min_qp2;
    int max_qp2;
    int record_video_stream;
};
#else
struct media_info
{
    int media_size;
    int bitrate;
    int media_ratemode;
    int key_frame;
    int quant;
};
#endif
struct media_info2
{
    int media_size2;
    int bitrate2;
    int media_ratemode2;
    int key_frame2;
    int quant2;
    int min_qp2;
    int max_qp2;
};

//for setting camera parames setting struct
struct command_info
{
    int type;
    struct media_info info;
};

//for common camera parames setting, contains
//contrastness, brightness, flip, framerate
struct camera_control_info
{
    int type;
    int value;
};

//for notify device info changed struct
//contains notify ie broswer client,
typedef struct notify_ie_broswer_command
{
    int type;
    unsigned int value;
}struct_notify_ie_broswser_command;

typedef struct
{
    unsigned char model[16];	// IPCam mode
    unsigned char vendor[16];	// IPCam manufacturer
    unsigned int version;		// IPCam firmware version	ex. v1.2.3.4 => 0x01020304;  v1.0.0.2 => 0x01000002
    unsigned int channel;		// Camera index
    unsigned int total;			// 0: No cards been detected or an unrecognizeable sdcard that could not be re-formatted.
                                // -1: if camera detect an unrecognizable sdcard, and could be re-formatted
                                // otherwise: return total space size of sdcard (MBytes)

    unsigned int free;			// Free space size of sdcard (MBytes)
    unsigned char reserved[8];	// reserved
}system_version_info;

#define	R_DAtA_FLAG		0x55
typedef struct RecordfileInfo {
    unsigned int  r_id;           //0x55
    unsigned int  r_time;       //1970到现在的秒数
    char r_file_name[24];         //20131028_140910.mpn
    unsigned int   r_interval;	//时长 毫秒为单位
    unsigned int   r_flen;		//文件大小字节数
    unsigned short r_type;		//触发类型： gpio报警10，移动侦测01，其他录像11, 定时录像00
    unsigned short r_channel;	//通道：主码流0、次码流1
}struct_record_file_info;

typedef enum event_type
{
    REASON_UNKNOW = 0,
    REASON_MOVEMENT = 1,
    REASON_GPIO     = 2
}enum_event_type;

typedef enum event_action_type
{
    ACTION_NONE   = 0,
    ACTION_GPIO	  =0x1,
    ACTION_PTZ	  =0x2,
    ACTION_RECORD =0x4,
    ACTION_EMAIL  =0x8,
    ACTION_FTP	  =0x10
}enum_event_action_type;

struct AlarmData{
    unsigned char data_flag;
    time_t rec_t;
    unsigned short reason;
    unsigned short action;
};

typedef struct uid
{
    char type[8];//product type "AH264", "CMJPEG"
    char uuid[24];
    int  reserver_[2];
}struct_uid;

struct upgrade_info
{
	int type;
	char upgrade_type[8];
	char filepath[16];
};

typedef struct firmware_info
{
    char upgrade_type[8];
    char filepath[16];
}update_firmware_info;

typedef struct
{
    unsigned int channel;                       // camera index
    int door1 ;                                 // 大門, 0:close, 1:open
    int door2 ;                                 // 小門, 0:close, 1:open
    int reserved ;                              // reserved, not used
}aide_door_params;


typedef enum alarm_notify_type
{
    ALARM_NOTIFY_TYPE_UNKNOW         =0,
    ALARM_NOTIFY_TYPE_MOTION         =1,
    ALARM_NOTIFY_TYPE_RESTORE_FACTORY=2,
    ALARM_NOTIFY_TYPE_WIFI_SUCCESS   =3,
    ALARM_NOTIFY_TYPE_WPS            =4,
    ALARM_NOTIFY_TYPE_LAN_SUCCES     =5,
    ALARM_NOTIFY_TYPE_WELCOM         =6,
    ALARM_NOTIFY_TYPE_ALARM_FAILED   =7,
    ALARM_NOTIFY_TYPE_ALARM_SUCCESS  =8,
    ALARM_NOTIFY_TYPE_UPGRADE_SUCCESS=9,
    ALARM_NOTIFY_TYPE_UPGRADING      =10,
    ALARM_NOTIFY_TYPE_LAN_FAILED     =11,
    ALARM_NOTIFY_TYPE_NET_GETTING    =12,
    ALARM_NOTIFY_TYPE_NET_AP         =13
}enum_alarm_notify_type;

typedef struct motion_happend
{
    int happend_time_second_;
    int happend_count_;
}struct_motion_happend;

//for wayde tech
#define	WAYDE_TECH_CMD_TYPE		            0x5a5a
struct smart_cmd_data {
    int type;
    unsigned char data[8];
};

typedef struct
{
    unsigned int  commd_type;
    unsigned char start; 	// start byte : always 0xF5
    unsigned char iid ;   	// 识别码 : 0x8B
    unsigned char fnCmd; 	//功能命令
    unsigned char high;  	//遥控地址高位
    unsigned char low;   	//遥控地址低位
    unsigned char deviceID;	//设备号
    unsigned char checkSum; // start xor iid xor fnCmd xor high xor low xordeviceID
    unsigned char reserved[1];
}notify_wayde;

typedef enum record_capacity
{
    TYPE_SDCARD_STAUTS_UNKOWN=1,
    TYPE_SDCARD_STAUTS_CAN_RECORD=2,
    TYPE_SDCARD_STAUTS_CANNOT_RECORD=3,
    TYPE_SDCARD_STAUTS_SHUTDOWN_PREP=4
}enum_record_capacity;

typedef enum notify_idealtech_type
{
     NOTIFY_SDCARD_STATUS = 80,
}enum_notify_idealtech;

#endif // COMMON_TYPES_H
