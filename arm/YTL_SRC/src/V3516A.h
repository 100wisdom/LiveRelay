

#include  "sample_comm.h"
#include  "v_ini.h"

#define VI_HDMI	1	// 采用HDMI作为信号源
#define VI_CVBS	2

extern HI_U8 g_ViSource;

HI_S32 StartVideoEnc();
HI_S32 StopVideoEnc();

HI_S32 SAMPLE_AUDIO_AiAenc(HI_VOID);
void UDP_THREAD(void);
void init_VideoParam(void);


extern ini_reader  reader;
