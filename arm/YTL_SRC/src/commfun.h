
/*
 * @Copyright (c)      GuangZhou JiaKe Inc., 2012. All Rights Reserved.
 *
 * @author             ZhangLisheng
 *
 * @description        common function of CGI
 *                          
 * @version            v1.0
 *
 * @history 
 *
 *
 */

#ifndef _COMMON_H
#define _COMMON_H
#ifdef __cplusplus
extern "C" {
#endif
#include "common_types.h"
int lock_set(int fd, int type);
int lock_file(int fd);
int unlock_file(int fd);
char *gogetenv(char *varname);
char **getCGIvars();
char **getFILEvars(char* pFileName, char pVarName[][MAX_NAME_SIZE], int varNum);
char *getValue(char **gVars, char *key);
char *setValue(char **gVars, char *key, char *newValue);
void freeVars(char **gVars);
void PRINTF_VARS(char **gVars);
int readStrFromFile(char* pFileName, int* pFd, char** ppFileStr);
int writeStrToFile(int fd, char* pFileStr);
int writeStrToTempFile(char *source_file, char* pFileStr);
char* getVarValFromStr(char* pFileStr, char* pVarName, int* pValLen);
int setVarValtoStr(char* pFileStr, char* pVarName, char* pVal);
int updateVarFile(char* pFileName, char pVarName[][MAX_NAME_SIZE], char** pVars, int varNum);
int checkLoginUserAndPas(char* pLoginuse, char* pLoginPas);
void refreshPage(char* pNextUrl);

#ifdef __cplusplus
}
#endif
#endif /* _COMMON_H */


