/*
 * CLiveDevice.h
 *
 *  Created on: 2016年4月28日
 *      Author: terry
 */

#ifndef CLIVEDEVICE_H_
#define CLIVEDEVICE_H_

#include "LiveDevice.h"
#include "MediaFormat.h"
#include "RtpCaster.h"
#include "TThread.h"
#include "TCriticalSection.h"
#include "TEvent.h"
#include "SharedPtr.h"

#include "RtpMediaFormat.h"

#include <client.h>
#include <messagehandler.h>
#include <connectionlistener.h>

#include "MediaChannel.h"
#include <vector>


using namespace gloox;



namespace av
{

class CLiveDevice:  public LiveDevice ,
					public comn::Thread,
					public ConnectionListener ,
					public IqHandler
{
public:
	CLiveDevice();
	virtual ~CLiveDevice();

	static void Startup();

	static void Cleanup();

	virtual void setLocalIp(const char* ip);

	virtual void setServer(const char* ip, const char* username, const char* password);

	virtual int open(int chl);

	virtual void close();

	virtual bool isOpen();

    virtual void setVideoFormat(int chl, int width, int height, int fps, int codec);

    virtual void setAudioFormat(int chl, int channels, int sampleRate, int codec);

	virtual void inputVideo(int chl, uint8_t* data, int size, int64_t pts);

	virtual void inputAudio(int chl, uint8_t* data, int size, int64_t pts);

protected:
	virtual int run();
	virtual void doStop();
	virtual bool startup();
	virtual void cleanup();

	virtual bool handleIq( const IQ& iq );
	virtual void handleIqID( const IQ& iq, int context );

	virtual	void onConnect();
	virtual	void onDisconnect( ConnectionError e );
	virtual	bool onTLSConnect( const CertInfo& info );

	void openClient();
	void closeClient();

	std::string makeUrl(const MediaFormat& fmt, std::string& audioUrl);
	std::string makeMediaUrl(const MediaFormat&, int videoPort, int audioPort, std::string& audioUrl);

	std::string getHostIP();

	std::string getJid() const;

	int getProtocol(const RtpMediaFormat& fmt);

protected:
    std::vector< MediaChannelPtr > m_channels;

	std::shared_ptr< Client >	m_client;
	comn::Event	m_event;

	std::string	m_localIp;
	std::string	m_server;
	std::string	m_username;
	std::string	m_password;

};



} /* namespace av */

#endif /* CLIVEDEVICE_H_ */
