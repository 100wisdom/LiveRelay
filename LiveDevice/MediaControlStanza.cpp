/*
 * MediaControlStanza.cpp
 *
 *  Created on: 2016年4月28日
 *      Author: terry
 */

#include "MediaControlStanza.h"
#include "TStringCast.h"



const std::string XMLNS_MEDIA_CONTROL        = "media:control";
const char*	MediaControlStanza::XMLNS = "media:control";
const char*  MediaControlStanza::COMMAND_DESC = "desc";
const char*  MediaControlStanza::COMMAND_SETUP = "setup";
const char*  MediaControlStanza::COMMAND_PLAY = "play";
const char*  MediaControlStanza::COMMAND_TEARDOWN = "teardown";


MediaControlStanza::MediaControlStanza(const Tag* tag):
		StanzaExtension(EXTENSION_TYPE),
		m_tag()
{
	if (tag)
	{
		m_tag = tag->clone();
	}
}

MediaControlStanza::~MediaControlStanza()
{
	delete m_tag;
}

const std::string& MediaControlStanza::filterString() const
{
	static const std::string filter = "/iq/query[@xmlns='" + XMLNS_MEDIA_CONTROL + "']";
	return filter;
}

StanzaExtension* MediaControlStanza::newInstance( const Tag* tag ) const
{
	return new MediaControlStanza(tag);
}

Tag* MediaControlStanza::tag() const
{
	return m_tag->clone();
}

StanzaExtension* MediaControlStanza::clone() const
{
	MediaControlStanza* stanza = new MediaControlStanza(m_tag->clone());
	return stanza;
}


std::string MediaControlStanza::getCommand() const
{
	if (!m_tag)
	{
		return std::string();
	}

	return m_tag->findAttribute("command");
}

void MediaControlStanza::setCommand(const std::string& cmd)
{
	Tag* tag = new Tag("query");
	tag->setXmlns(XMLNS_MEDIA_CONTROL);
	tag->addAttribute("command", cmd);

	if (m_tag)
	{
		delete m_tag;
		m_tag = NULL;
	}

	m_tag = tag;
}

bool MediaControlStanza::getUrl(std::string& videoUrl, std::string& audioUrl) const
{
	if (!m_tag)
	{
		return "";
	}

	Tag* tag = m_tag->findChild("media");
	if (!tag)
	{
		return "";
	}

	videoUrl = tag->findAttribute("video");
	audioUrl = tag->findAttribute("audio");
	return videoUrl.empty() || audioUrl.empty();
}

void MediaControlStanza::setUrl(const std::string& videoUrl, const std::string& audioUrl)
{
	Tag* tag = new Tag("media");
	tag->addAttribute("video", videoUrl);
	tag->addAttribute("audio", audioUrl);
	m_tag->addChild(tag);
}

void MediaControlStanza::setChannel(const std::string& chl)
{
    if (m_tag)
    {
        m_tag->addAttribute("channel", chl);
    }
    else
    {
        //
    }
}

const std::string MediaControlStanza::getChannel() const
{
    if (!m_tag)
    {
        return std::string();
    }

    return m_tag->findAttribute("channel");
}


