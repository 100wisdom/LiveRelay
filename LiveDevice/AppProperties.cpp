/*
 * AppProperties.cpp
 *
 *  Created on: 2015年12月22日
 *      Author: terry
 */

#include "AppProperties.h"
#include "IniProperties.h"
#include "TSafeStr.h"
#include "Path.h"


#undef  MAX_PATH


static const size_t MAX_PATH = 512;

static char	s_filename[MAX_PATH] = "app.ini";


AppProperties::AppProperties()
{
}

AppProperties::~AppProperties()
{
}

comn::IniProperties& AppProperties::getProperties()
{
	static comn::IniProperties s_properties;
	return s_properties;
}

void AppProperties::setPath(const char* filename)
{
    comn::copyStr(s_filename, filename);
	//strcpy(s_filename, filename);

    std::string fullpath = getPath();
    getProperties().load(fullpath.c_str());
}

std::string AppProperties::getPath()
{
	std::string paths[] =
	{
		".",

#ifdef WIN32
		comn::Path::getWorkDir(NULL),
#else
		"/usr/etc",
		"/etc",
#endif //

	};

	return comn::Path::findFile(paths, sizeof(paths)/sizeof(paths[0]), s_filename);
}



