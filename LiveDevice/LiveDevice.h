/*
 * LiveDevice.h
 *
 *  Created on: 2016年4月28日
 *      Author: terry
 */

#ifndef LIVEDEVICE_H_
#define LIVEDEVICE_H_

#include "BasicType.h"

namespace av
{

class DLLEXPORT LiveDevice
{
public:
	LiveDevice();
	virtual ~LiveDevice();

	static void startup();

	static void cleanup();

	static LiveDevice& instance();

	/*
	 * 设置本机地址, 在 Open 之前调用 
	*/
	virtual void setLocalIp(const char* ip) = 0;

	/*
	* 设置服务地址与账户, 在 open 之前调用 
	*/
	virtual void setServer(const char* ip, const char* username, const char* password) = 0;


	virtual int open(int chlCount) =0;

	virtual void close() =0;

	virtual bool isOpen() =0;


    virtual void setVideoFormat(int chl, int width, int height, int fps, int codec) =0;

    virtual void setAudioFormat(int chl, int channels, int sampleRate, int codec) =0;

	/**
	 * 输入视频包, 时间戳的单位为 1/1000000
	 */
	virtual void inputVideo(int chl, uint8_t* data, int size, int64_t pts) =0;

	/**
	 * 输入音频包, 时间戳单位为 1/1000000
	 */
	virtual void inputAudio(int chl, uint8_t* data, int size, int64_t pts) =0;


};



} /* namespace av */

#endif /* LIVEDEVICE_H_ */
