/*    file: MediaChannel.cpp
 *    desc:
 * 
 * created: 2016-12-04
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include "MediaChannel.h"

namespace av
{


MediaChannel::MediaChannel():
    m_format(),
    m_videoCaster(),
    m_audioCaster(),
    m_ssrc(),
    m_videoPort(),
    m_audioPort(),
    m_protocol()
{
    m_ssrc = reinterpret_cast<intptr_t>((void*)this);
}

MediaChannel::~MediaChannel()
{
    close();
}

void MediaChannel::setVideoFormat(int width, int height, int fps, int codec)
{
    m_format.m_width = width;
    m_format.m_height  = height;
    m_format.m_framerate = fps;
    m_format.m_codec = codec;

    if (!m_videoCaster)
    {
        rtpcaster_create_channel(&m_videoCaster, m_format.m_codec, 90000, m_videoPort, m_ssrc);
    }
}

void MediaChannel::setAudioFormat(int channels, int sampleRate, int codec)
{
    m_format.m_channels = channels;
    m_format.m_sampleRate = sampleRate;
    m_format.m_audioCodec = codec;

    if (m_format.m_audioCodec > 0 && !m_audioCaster)
    {
        rtpcaster_create_channel(&m_audioCaster, m_format.m_audioCodec, m_format.m_sampleRate, m_audioPort, m_ssrc);
    }
}

void MediaChannel::setPort(int videoPort, int audioPort)
{
    m_videoPort = videoPort;
    m_audioPort = audioPort;
}

int MediaChannel::open()
{
    return 0;
}

void MediaChannel::close()
{
    if (m_videoCaster != NULL)
    {
        rtpcaster_destroy_channel(m_videoCaster);
        m_videoCaster = NULL;
    }

    if (m_audioCaster != NULL)
    {
        rtpcaster_destroy_channel(m_audioCaster);
        m_audioCaster = NULL;
    }
}

void MediaChannel::inputVideo(uint8_t* data, int size, int64_t pts)
{
    int64_t videoPts = pts * 90000 / 1000000;
    rtpcaster_write_data(m_videoCaster, data, size, videoPts, 0);
}

void MediaChannel::inputAudio(uint8_t* data, int size, int64_t pts)
{
    int64_t audioPts = pts * m_format.m_sampleRate / 1000000;
    rtpcaster_write_data(m_audioCaster, data, size, audioPts, 0);
}

const MediaFormat& MediaChannel::getFormat()
{
    return m_format;
}

void MediaChannel::addTarget(int mediaType, int protocol, const std::string& ip, int port)
{
    if (mediaType == MEDIA_TYPE_VIDEO)
    {
        rtpcaster_add_target(m_videoCaster, protocol, ip.c_str(), port);
    }
    else if (mediaType == MEDIA_TYPE_AUDIO)
    {
        rtpcaster_add_target(m_audioCaster, protocol, ip.c_str(), port);
    }
}

void MediaChannel::removeTarget(int mediaType, int protocol)
{
    if (mediaType == MEDIA_TYPE_VIDEO)
    {
        rtpcaster_remove_all_target(m_videoCaster, protocol);
    }
    else if (mediaType == MEDIA_TYPE_AUDIO)
    {
        rtpcaster_remove_all_target(m_audioCaster, protocol);
    }
}

void MediaChannel::removeTarget(int mediaType)
{
    if (mediaType == MEDIA_TYPE_VIDEO)
    {
        rtpcaster_remove_all_target(m_videoCaster, kProtocolUdp);
        rtpcaster_remove_all_target(m_videoCaster, kProtocolTcp);
    }
    else if (mediaType == MEDIA_TYPE_AUDIO)
    {
        rtpcaster_remove_all_target(m_audioCaster, kProtocolUdp);
        rtpcaster_remove_all_target(m_audioCaster, kProtocolTcp);
    }
}

int MediaChannel::getVideoPort() const
{
    return m_videoPort;
}

int MediaChannel::getAudioPort() const
{
    return m_audioPort;
}



}