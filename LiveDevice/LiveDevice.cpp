/*
 * LiveDevice.cpp
 *
 *  Created on: 2016年4月28日
 *      Author: terry
 */

#include "LiveDevice.h"
#include "TSingleton.h"
#include "CLiveDevice.h"



namespace av
{

typedef comn::Singleton< CLiveDevice >		LiveDeviceSingleton;


LiveDevice::LiveDevice()
{
}

LiveDevice::~LiveDevice()
{
}

void LiveDevice::startup()
{
	CLiveDevice::Startup();
}

void LiveDevice::cleanup()
{
	CLiveDevice::Cleanup();
}

LiveDevice& LiveDevice::instance()
{
	return LiveDeviceSingleton::instance();
}




} /* namespace av */
