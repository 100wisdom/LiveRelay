/*
 * MediaCodecName.cpp
 *
 *  Created on: 2016年5月1日
 *      Author: terry
 */

#include "MediaCodecName.h"

namespace av
{

MediaCodecName::MediaCodecName()
{
}

MediaCodecName::~MediaCodecName()
{
}

struct CodecName
{
	int codec;
	const char* name;
};

static CodecName s_codecNames[] =
{
	{MEDIA_CODEC_H264, "H264"},
	{MEDIA_CODEC_VP8, "VP8"},
	{MEDIA_CODEC_MPEG4, "MPEG4"},
	{MEDIA_CODEC_THEORA, "THEORA"},
	{MEDIA_CODEC_HEVC, "HEVC"},
	{MEDIA_CODEC_G711U, "PCMU"},
	{MEDIA_CODEC_G711A, "PCMA"},
	{MEDIA_CODEC_MP3, "MP3"},
	{MEDIA_CODEC_AAC, "AAC"},
	{MEDIA_CODEC_AC3, "AC3"},
	{MEDIA_CODEC_VORBIS, "VORBIS"},
    { MEDIA_CODEC_VORBIS, "OPUS" },
	{MEDIA_CODEC_RAW, "RAW"},
};

const char* MediaCodecName::getName(int codec)
{
	for (size_t i = 0; i < sizeof(s_codecNames)/sizeof(s_codecNames[0]); i ++)
	{
		if (codec == s_codecNames[i].codec)
		{
			return s_codecNames[i].name;
		}
	}
	return "";
}

int MediaCodecName::codecFromName(const std::string& name)
{
	for (size_t i = 0; i < sizeof(s_codecNames)/sizeof(s_codecNames[0]); i ++)
	{
		if (name == s_codecNames[i].name)
		{
			return s_codecNames[i].codec;
		}
	}
	return MEDIA_CODEC_NONE;
}


} /* namespace av */
