/*    file: MediaChannel.h
 *    desc:
 *   
 * created: 2016-12-04
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined MEDIACHANNEL_H_
#define MEDIACHANNEL_H_

#include "MediaFormat.h"
#include "RtpCaster.h"
#include "TThread.h"
#include "TCriticalSection.h"
#include "TEvent.h"
#include "SharedPtr.h"

#include "RtpMediaFormat.h"


namespace av
{



class MediaChannel
{
public:
    MediaChannel();
    ~MediaChannel();

    void setVideoFormat(int width, int height, int fps, int codec);

    void setAudioFormat(int channels, int sampleRate, int codec);

    void setPort(int videoPort, int audioPort);
    int open();

    void close();

    void inputVideo(uint8_t* data, int size, int64_t pts);
    void inputAudio(uint8_t* data, int size, int64_t pts);

    const MediaFormat& getFormat();

    void addTarget(int mediaType, int protocol, const std::string& ip, int port);

    void removeTarget(int mediaType, int protocol);

    void removeTarget(int mediaType);

    int getVideoPort() const;
    int getAudioPort() const;

protected:
    MediaFormat m_format;
    HANDLE	m_videoCaster;
    HANDLE	m_audioCaster;
    int		m_ssrc;
    int		m_videoPort;
    int		m_audioPort;
    int	m_protocol;

};

typedef std::shared_ptr< MediaChannel >     MediaChannelPtr;


}

#endif //MEDIACHANNEL_H_

