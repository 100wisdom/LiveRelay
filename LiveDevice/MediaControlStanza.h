/*
 * MediaControlStanza.h
 *
 *  Created on: 2016年4月28日
 *      Author: terry
 */

#ifndef MEDIACONTROLSTANZA_H_
#define MEDIACONTROLSTANZA_H_

#include <gloox.h>
#include <stanzaextension.h>
#include <tag.h>
using namespace gloox;



extern const std::string XMLNS_MEDIA_CONTROL;


class MediaControlStanza : public StanzaExtension
{
public:
	static const int EXTENSION_TYPE = gloox::ExtUser + 2;

	static const char*	XMLNS;
	static const char*  COMMAND_DESC;
    static const char*  COMMAND_SETUP;
    static const char*  COMMAND_PLAY;
    static const char*  COMMAND_TEARDOWN;

public:
	explicit MediaControlStanza(const Tag* tag);
	virtual ~MediaControlStanza();

	virtual const std::string& filterString() const;

	virtual StanzaExtension* newInstance( const Tag* tag ) const;

	virtual Tag* tag() const;

	virtual StanzaExtension* clone() const;


	std::string getCommand() const;

	void setCommand(const std::string& cmd);

	bool getUrl(std::string& videoUrl, std::string& audioUrl) const;

	void setUrl(const std::string& videoUrl, const std::string& audioUrl);

    void setChannel(const std::string& chl);

    const std::string getChannel() const;

protected:
	Tag*	m_tag;

};




#endif /* MEDIACONTROLSTANZA_H_ */
